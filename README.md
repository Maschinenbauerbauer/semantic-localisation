# Semantic Localisation

This project contains all ROS2 Nodes of the master thesis of Andreas Bauer

This was used to create the software foundation for a mobile robot quadrocopter drone, to be able to localise in its surrounding.
The Navigation part was not part of this master thesis. For now, the object detection, segmentation, mapping and localisation have been implemented.

The software package allows to use a ZED2 camera to create a map of the surrounding and afterwards use this map to do localisation tasks.
There were various localisation methods implemented, but only a particle filter seemed usable.

This project can be build by the default methods stated on the ROS2-foxy documentation. 
An installed ZED SDK is needed. 

Keep in mind:
This repo is for demonstration purposes only and is not maintained anymore!
