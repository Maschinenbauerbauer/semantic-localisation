#!/usr/bin/env python

import rclpy
from rclpy.node import Node
from rclpy.time import Duration
from rclpy.qos import qos_profile_sensor_data

# Modules & Nodes
import ros2_numpy
import numpy as np
import sensor_msgs_py.point_cloud2 as pc2

# Msgs
from sensor_msgs.msg import PointCloud2
from visualization_msgs.msg import Marker, MarkerArray
from msgs.msg import ObjectArray
from geometry_msgs.msg import Point

class Visualizer(Node):

    def __init__(self):
        super().__init__('visualizer')

        self.segmented_objects_3d_sub = self.create_subscription(
            ObjectArray,
            "segmentation/segmented_detections",
            self.callback,
            qos_profile_sensor_data
        )
        
        self.pub_markers = self.create_publisher(
            MarkerArray,
            "segmentation_viz/bounding_box_3d/marker_array", # topic names must not start with number!!
            1
        )

        self.pub_segmented_pointcloud = self.create_publisher(
            PointCloud2,
            "segmentation_viz/segmented_pointcloud", 
            1
        )

        self._segmented_pointclouds_np = np.asarray([])


    def generate_marker_cube_skeleton(self, header, obj, num):
        """
        Generates a marker cube made out of lines so that the object within remains visible.

        :param header: The header.
        :param obj: The object.
        :param num: The ID.
        :param scale: The line-width.
        :return: A ROS message with the LINE_LIST that makes up the cube-skeleton around the object.
        """
        msg = Marker()
        msg.header = header
        msg.type = msg.LINE_LIST
        msg.action = msg.ADD
        msg.id = num
        msg.lifetime = Duration(seconds=0.5).to_msg()

        msg.scale.x = 0.01
        msg.scale.y = 0.01
        msg.scale.z = 0.01

        msg.color.a = 0.6
        msg.color.r = 0.0
        msg.color.g = 1.0
        msg.color.b = 0.0

        # Shape of skeleton:
        #    7 -------- 6
        #   /|         /|
        #  / |        / |
        # 4 -------- 5  |
        # |  |       |  |
        # |  3 ------|- 2        z
        # | /        | /         |  y
        # |/         |/          | /
        # 0 -------- 1           |/____ x

        # Create Points:
        bb3d_py = list(obj.bb3d)
        p0 = bb3d_py[0]
        p1 = bb3d_py[1]
        p2 = bb3d_py[2]
        p3 = bb3d_py[3]
        p4 = bb3d_py[4]
        p5 = bb3d_py[5]
        p6 = bb3d_py[6]
        p7 = bb3d_py[7]

        # Add edges to msg:
        # Point pair for each edge to draw
        msg.points.append(p0)  
        msg.points.append(p1) 

        msg.points.append(p1)  
        msg.points.append(p2) 
        
        msg.points.append(p2)  
        msg.points.append(p3) 

        msg.points.append(p3)  
        msg.points.append(p0) 
        
        msg.points.append(p0)  
        msg.points.append(p4)  
        
        msg.points.append(p1)  
        msg.points.append(p5)  

        msg.points.append(p2)  
        msg.points.append(p6)  

        msg.points.append(p3)
        msg.points.append(p7)

        msg.points.append(p4)
        msg.points.append(p5)

        msg.points.append(p5)
        msg.points.append(p6)

        msg.points.append(p6)
        msg.points.append(p7)

        msg.points.append(p7)
        msg.points.append(p4)
        
        #print(msg)
        return msg

    def callback(self, segmented_detections):
        """Create a marker around each object. Add all the pointclouds of the objects 
        into one np.array and publish them."""
        
        marker_arrays = MarkerArray()
        
        # Generate markers for each object in the segmented_detections.
        # Additionally, add all the points of the pointclouds of the objects in order into one np array.
        for num, obj in enumerate(segmented_detections.objects):
            if(obj.position3d[0] != 0.0 and obj.position3d[1] != 0.0 and obj.position3d[2] != 0.0):

                marker = self.generate_marker_cube_skeleton(segmented_detections.header, obj, num)
                marker_arrays.markers.append(marker) 
   
                if len(self._segmented_pointclouds_np) == 0:
                    self._segmented_pointclouds_np = ros2_numpy.numpify(obj.pc)

                else:
                    self._segmented_pointclouds_np = np.concatenate((self._segmented_pointclouds_np,
                                                                 	ros2_numpy.numpify(obj.pc)),
                                                                	axis=0)


        header = segmented_detections.header
        segmented_pointclouds = pc2.create_cloud_xyz32(header, self._segmented_pointclouds_np)

        # Publish the array with all pointclouds in order, reset that array, then publish the markers.
        self.pub_segmented_pointcloud.publish(segmented_pointclouds)   
        self._segmented_pointclouds_np = np.asarray([])  
        #print(marker_arrays)
        self.pub_markers.publish(marker_arrays)
        

def main(args=None):
    rclpy.init(args=args)
    node = Visualizer()
    rclpy.spin(node)
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
