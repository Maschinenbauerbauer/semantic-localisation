import rclpy
from rclpy.node import Node
from std_msgs.msg import String
# Messages
from sensor_msgs.msg import Image
from msgs.msg import ObjectArray, PixelCoordinates, PixelSegmentation, PixelSegmentationArray
# External
import numpy as np
import cv2
from cv_bridge import CvBridge

import pixellib
from pixellib.torchbackend.instance import instanceSegmentation

class PixelSegment(Node):

    def __init__(self):
        super().__init__('pixel_segment')
        self.image_publisher = self.create_publisher(Image, '/pixel_segment/image', 10)
        self.array_publisher = self.create_publisher(PixelSegmentationArray, '/pixel_segment/coordinates_array', 10)
        
        self.subscription = self.create_subscription(
            Image,
            'zed2/zed_node/left_raw/image_raw_color',
            self.listener_callback,
            1
        )

        self.bb_subscription = self.create_subscription(
            ObjectArray,
            'yolov5/detections_yolo',
            self.bb_callback,
            10
        )

        self.last_objectarray = None

        self.subscription  # prevent unused variable warning
        self.cv_bridge = CvBridge()

    def bb_callback(self, msg):
        self.last_objectarray = msg

    def listener_callback(self, img_msg):
        if self.last_objectarray:
            cv_img = self.cv_bridge.imgmsg_to_cv2(img_msg, desired_encoding='rgb8')
            #print(cv_img.shape)
            px_array = PixelSegmentationArray()
            px_array.header = img_msg.header
            # Entpacke BoundingBoxes Array 
            accumulated_mask = np.empty((cv_img.shape[0], cv_img.shape[1]),np.int32)


            bgdModel = np.zeros((1,65),np.float64)
            fgdModel = np.zeros((1,65),np.float64)
            
            for obj in self.last_objectarray.objects:
                coordinate_msg = PixelSegmentation()

                bounding_box = obj.bb
                classid = obj.objectclass

                coordinate_msg.objectclass = classid

                crop_img = cv_img[
                    bounding_box[1]:bounding_box[3], 
                    bounding_box[0]:bounding_box[2]
                    ]

                self.get_logger().info('Starting segmentation..')

                # Wenn Bild zu groß, runterskalieren!
                if crop_img.shape[0]>200 or crop_img.shape[1]>300:
                    scale_percent =  0.6# percent of original size
                    width = int(crop_img.shape[1] * scale_percent)
                    height = int(crop_img.shape[0] * scale_percent)
                    dim = (width, height)
                    # resize image
                    resized = cv2.resize(crop_img, dim, interpolation = cv2.INTER_AREA)
                    mask = np.zeros(resized.shape[:2],np.uint8)
                    rect = (1,1, int(crop_img.shape[1]*scale_percent-1), int(crop_img.shape[0]*scale_percent-1))

                    cv2.grabCut(resized,mask,rect,bgdModel,fgdModel,10,cv2.GC_INIT_WITH_RECT)

                    mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
                    mask2 = cv2.resize(mask2, (int(crop_img.shape[1]), int(crop_img.shape[0])), interpolation = cv2.INTER_AREA)
                    img = crop_img*mask2[:,:,np.newaxis]

                else:
                    mask = np.zeros(crop_img.shape[:2],np.uint8)
                    rect = [1,1, crop_img.shape[1]-1, crop_img.shape[0]-1]
                    cv2.grabCut(crop_img,mask,rect,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_RECT)

                    mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
                    img = crop_img*mask2[:,:,np.newaxis]

                cv2.imshow('img', img)
                cv2.waitKey(10000)

                self.get_logger().info('Finished segmentation..')
    
                #accumulated_mask = np.where(accumulated_mask>=1, 1, 0)
                content_indices = np.where(mask2 == 1)

                #self.get_logger().info(str(accumulated_mask))

                listOfCoordinates= list(zip(content_indices[0], content_indices[1]))

                #self.get_logger().info(str(listOfCoordinates))
                
                #print(listOfCoordinates)
                for tuple in listOfCoordinates:
                    x, y = tuple[0], tuple[1]
                    pxcoord = PixelCoordinates()
                    pxcoord.x, pxcoord.y = int(x), int(y)
                    #print(pxcoord.x)
                    coordinate_msg.coordinates.append(pxcoord)

                px_array.segment_array.append(coordinate_msg)

            self.get_logger().info('Starting publishing..')
            self.array_publisher.publish(px_array)
            self.get_logger().info('Finished publishing..')
            pass


def main(args=None):
    rclpy.init(args=args)

    node = PixelSegment()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()