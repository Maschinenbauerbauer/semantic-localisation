"""
Created on Tue Feb 16 17:01:13 2021

@author: ch
"""

import rclpy
import numpy as np
import message_filters
from msgs.msg import ObjectArray, ObjectInformation
#from numba import njit

class FusionNode(Node):

    def __init__(self):
        super().__init__('fusion_node')

        # Create two subscribers and a publisher
        sub_1 = message_filters.Subscriber("/detections_yolo", ObjectArray, buff_size=2**20)
        sub_2 = message_filters.Subscriber("/detections_motion", ObjectArray, buff_size=2**20)
        pub = rclpy.Publisher("/detections_fusion", ObjectArray, queue_size=1)
        
        # Synchronize the messages that the two subscribers receive.
        # When both got a message, a callback (callback_fusion) is called 
        # with the messages from the subscribers, as well as the publisher so that the 
        # fusion of the messages can be published.
        synchronizer = message_filters.ApproximateTimeSynchronizer([sub_1, sub_2], queue_size=20, slop=0.05, allow_headerless=False)
        synchronizer.registerCallback(callback_fusion, pub)
        # Done
        print("Detection Fusion running")
        

# TODO: why is this not jitted? 
#@njit
def iou_calc(A, B):
    """
    Calculates how much of the given bboxes overlap as a percentage of 
    the total area that they cover.

    :param A: Bounding box, consisting of [UpperLeft_x, UpperLeft_y, LowerRight_x, LowerRight_y].
    :param B: Bounding box.

    :returns: iou, the intersecting area's part of the total area covered by A and B in percent/100.
    """
    xA = max(A[0], B[0])
    yA = max(A[1], B[1])
    xB = min(A[2], B[2])
    yB = min(A[3], B[3])

    # compute the area of intersection rectangle
    interArea = max(0, xB - xA) * max(0, yB - yA)

    if interArea == 0:
        return 0.0

    # compute the area of both the prediction and ground-truth rectangles
    boxAArea = (A[2] - A[0]) * (A[3] - A[1])
    boxBArea = (B[2] - B[0]) * (B[3] - B[1])

    iou = interArea / float(boxAArea + boxBArea - interArea)
    return iou


# TODO: The for-loop in this function is fully parallelizable; would it make sense to @njit(parallel=True) this? 
def find_matching_box(bb_list_yolo, bb_motion, match_iou):
    """
    A bounding box from one detection of one algorithm is compared to 
    the bounding boxes of the other algorithms detections. The best fit is returned.

    :param bb_list_yolo: The bounding boxes of the objects classified by Yolo.
    :param bb_motion: The bounding box of one object classified by FastMCD.
    :param match_iou: Minimum iou for two objects to be considered a match.

    :returns: The index and IOU of the best fit.
    """
    best_iou = match_iou
    best_index = -1
    for i in range(len(bb_list_yolo)):
        iou = iou_calc(bb_list_yolo[i].bb, bb_motion)
        if iou > best_iou:
            best_index = i
            best_iou = iou
    return best_index, best_iou

def get_weighted_box(cluster):
    """
    Get the confidence-weighted average of a bunch of bounding boxes.

    :param cluster: a list of detected objects.
    :returns: result, an object with high confidence whose bounding box is the confidence weighted avg of those in cluster.
    """
    
    result = ObjectInformation()
    bb = np.array([0., 0., 0., 0.])
    conf = 0          # For normalization of confidence-weighted sum.
    conf_list = []    # List of all confidence values in cluster; its max will be result's confidence.
    class_list = []
    result.is_dynamic = False 
    
    for b in cluster:
        box = np.asarray([float(i) for i in b.bb])
        bb[:] += (b.confidence * box)
        conf += b.confidence
        conf_list.append(b.confidence)
        class_list.append(b.object_class)
        if b.object_class == "dynamic":
            result.is_dynamic = True 
        
    result.object_class = class_list[np.argmax(np.array(conf_list))]
    result.confidence = np.array(conf_list).max()    
    bb[:] /= conf
    result.bb = [int(round(i)) for i in bb]
    return result
  
def callback_fusion(bb_list_yolo, bb_list_motion, pub): 
    """
    Fuses the data from Yolo and FastMCD and publishes it to the publisher pub.

    Is a callback that is called when there is a message from both the 
    Yolo- and the FastMCD-Subscribers.

    :param bb_list_yolo: the message from Yolo.
    :param bb_list_motion: the message from FastMCD.
    :param pub: The publisher to which the fused message should be published.
    """

    recognized_objects = ObjectArray()
    recognized_objects.header = bb_list_yolo.header
    
    # Initialize an array of the same length as the nr of objects FastMCD detected; initialize all entries to -1.
    matching_indices = [-1] * len(bb_list_motion.objects)

    # Go through every object that FastMCD found.
    # If Yolo found the same object, the detections are merged to improve accuracy. 
    # To do this, save the index of the matching Yolo object in matching_indices[index of matching FastMCD object.].
    # If Yolo didn't find the same object, then just save the FastMCD object in recognized_objects.
    for n in range(len(bb_list_motion.objects)):   
        # Did Yolo detect the nth object that FastMCD detected, too?
        best_index, best_iou = find_matching_box(bb_list_yolo.objects, 
                                                 bb_list_motion.objects[n].bb,
                                                 0.01)

        # No match found --> Yolo and FastMCD didn't both detect the same object.
        # Use the recognized object from FastMCD in recognized_objects.
        if best_index == -1:
           bb_list_motion.objects[n].is_dynamic = True
           recognized_objects.objects.append(bb_list_motion.objects[n])         
        # Match found --> Index saved for later use.
        else:
           matching_indices[n] = best_index

    # Merge all matching boxes.
    # All FastMCD objects that do not have a corresponding Yolo object have already been added to recognized_objects;
    # add all Yolo objects without a matching FastMCD object, too.
    for i in range(len(bb_list_yolo.objects)):

        # Create a list of all objects from FastMCD who have a good match with the ith object from Yolo.
        matching_boxes = []
        for l in range(len(matching_indices)):
            # i is the index of the best fit of a Yolo object to the FastMCD object at index l
            if matching_indices[l] == i:   
                matching_boxes.append(bb_list_motion.objects[l])

        if not matching_boxes:
            recognized_objects.objects.append(bb_list_yolo.objects[i])
        else: 
            matching_boxes.append(bb_list_yolo.objects[i])
            fusion = get_weighted_box(matching_boxes)
            recognized_objects.objects.append(fusion)
         
    pub.publish(recognized_objects)


if __name__ == "__main__":
    rclpy.init(args=args)
    node = FusionNode()

    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()
    
