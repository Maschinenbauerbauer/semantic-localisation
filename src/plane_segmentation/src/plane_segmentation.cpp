// Include the C++ standard library headers

#define BOOST_BIND_NO_PLACEHOLDERS

#include <memory> // Dynamic memory management
 
// Dependencies
#include "rclcpp/rclcpp.hpp" // ROS Clienty Library for C++
#include "std_msgs/msg/string.hpp" // Handles String messages in ROS 2
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <pcl_conversions/pcl_conversions.h> 
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

using std::placeholders::_1;

class PlaneSegmentation : public rclcpp::Node
{
  public:
    // Constructor
    // The name of the node is minimal_subscriber
    PlaneSegmentation()
    : Node("minimal_subscriber")
    {
      // Create the subscription.
      // The topic_callback function executes whenever data is published
      // to the 'addison' topic.
      plane_publisher_ = this->create_publisher<sensor_msgs::msg::PointCloud2>("/planesegmentation/detected_plane", 1);
      pc_publisher_ = this->create_publisher<sensor_msgs::msg::PointCloud2>("/planesegmentation/point_cloud", 1);
      subscription_ = this->create_subscription<sensor_msgs::msg::PointCloud2>(
      "/zed2/zed_node/point_cloud/cloud_registered", 1, std::bind(&PlaneSegmentation::topic_callback, this, _1));
    }
 
  private:
    // Receives the String message that is published over the topic
    void topic_callback(const sensor_msgs::msg::PointCloud2::SharedPtr msg) const
    {
      std::cerr << "Point cloud received" << std::endl;

      pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
      pcl::fromROSMsg(*msg, *cloud);

      pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
      pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
      // Create the segmentation object
      pcl::SACSegmentation<pcl::PointXYZ> seg;
      // Optional
      seg.setOptimizeCoefficients (false);
      seg.setMaxIterations(20);
      seg.setNumberOfThreads(8);
      // Mandatory
      seg.setModelType (pcl::SACMODEL_PLANE);
      seg.setMethodType (pcl::SAC_RANSAC);
      seg.setDistanceThreshold (0.1);

      seg.setInputCloud (cloud);
      seg.segment(*inliers, *coefficients);

      if (inliers->indices.size () == 0)
      {
          PCL_ERROR ("Could not estimate a planar model for the given dataset.\n");
      }

      // Output Plane
      pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out(new pcl::PointCloud<pcl::PointXYZ>);
      pcl::copyPointCloud(*cloud, *inliers, *cloud_out);

      sensor_msgs::msg::PointCloud2 ros_plane_out;
      pcl::toROSMsg(*cloud_out, ros_plane_out);
      plane_publisher_->publish(ros_plane_out);

      // Filter out plane from original point cloud
      pcl::ExtractIndices<pcl::PointXYZ> extract;
      pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>);
      extract.setInputCloud(cloud);
      extract.setIndices(inliers);
      extract.setNegative(true);
      extract.filter(*filtered_cloud);

      sensor_msgs::msg::PointCloud2 ros_pc_out;
      pcl::toROSMsg(*filtered_cloud, ros_pc_out);
      pc_publisher_->publish(ros_pc_out);
    }

    // Declare the subscription attribute
    rclcpp::Subscription<sensor_msgs::msg::PointCloud2>::SharedPtr subscription_;
    rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr plane_publisher_;
    rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr pc_publisher_;
};
 
int main(int argc, char * argv[])
{
  // Launch ROS 2
  rclcpp::init(argc, argv);
   
  // Prepare to receive messages that arrive on the topic
  rclcpp::spin(std::make_shared<PlaneSegmentation>());
   
  // Shutdown routine for ROS2
  rclcpp::shutdown();
  return 0;
}
