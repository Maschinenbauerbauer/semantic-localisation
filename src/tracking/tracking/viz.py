#!/usr/bin/env python
# coding=utf-8

import numpy as np
import cv2
from collections import deque

import rospy
import message_filters
from oo_msgs.msg import object_array
from cv_bridge import CvBridge
from sensor_msgs.msg import Image

DRAW_MOTION_PATH = True

# TODO: it is best practice to not reseed the BitGenerator, but to create a new one.
np.random.seed(100)
COLORS = np.random.randint(0, 255, size=(200, 3), dtype="uint8")

# Deque: Similar to list, but optimized for appending and popping to and from beginning and end of the deque.
# Each deque can contain points along a path that an object travelled.
pts = [deque(maxlen=30) for _ in range(9999)]

class TrackingVisualization:
    def __init__(self):
    
        self._objects_sub = message_filters.Subscriber("/detections_tracking",
                                                               object_array,  buff_size=2**15)
        self._image_sub = message_filters.Subscriber("/camera/color/image_raw",
                                                               Image,  buff_size=2**15)
        self._sync = message_filters.ApproximateTimeSynchronizer([self._objects_sub, self._image_sub],
                                                                 queue_size=100, slop=0.05, allow_headerless=False)
        self._sync.registerCallback(self.multi_callback)
        self._bridge = CvBridge()

    def multi_callback(self, tracked_objects, image_with_bbox_ros_msg):
        """
        Add a box and descriptive text for each object to the raw image.

        If DRAW_MOTION_PATH, the path that each object has travelled so far will be shown as well.

        :param tracked_objects: The tracked objects, used to draw boxes around each detected object and information about it.
        :param image_with_bbox_ros_msg: The raw image.
        """
        

        # TODO: either have image_with_bbox_ros_msg already have the bboxes drawn into it and don't redraw them
        # or rename the variable.
        
        global pts
        
        objects = tracked_objects.objects

        image = self._bridge.imgmsg_to_cv2(image_with_bbox_ros_msg, "bgr8")

        for obj in objects:

            track_id = obj.tracking_id
            left, top, right, bottom = obj.bb
            
            if track_id == 0:
                cv2.rectangle(image, (left, top), (right, bottom), [0, 255, 0], 4)
                cv2.putText(image, "{}".format(obj.object_class), (left+5, top+20), cv2.FONT_HERSHEY_SIMPLEX, 0.4, [0, 255, 0], 1)
            else:
                _track_id = track_id % len(COLORS)
                #color = [int(c) for c in COLORS[_track_id]]
                
                cv2.rectangle(image, (left, top), (right, bottom), [0,255,0], 4)
                cv2.putText(image, "{}".format(track_id), (left+5, top+20), cv2.FONT_HERSHEY_SIMPLEX, 0.4, [0,255,0], 1)
                cv2.putText(image, "{}".format(obj.object_class), (left+25, top+20), cv2.FONT_HERSHEY_SIMPLEX, 0.4, [0,255,0], 1)
                
                if DRAW_MOTION_PATH:
                    # Append the center of the current object to the path with its track_id and draw the path as a 
                    # succession of lines.
                    center = (int((left + right) / 2), int((top + bottom) / 2))
                    pts[track_id].append(center)

                    # Current object
                    cv2.circle(image, center, 1, [0, 0, 255], 2)

                    # Draw path
                    for j in range(1, len(pts[track_id])):
                        if pts[track_id][j - 1] is None or pts[track_id][j] is None:
                            pass
                        else:
                            cv2.line(image, (pts[track_id][j - 1]), (pts[track_id][j]), [0, 0, 255], 2)
                        
        cv2.imshow("Tracking", image)
        cv2.waitKey(1)

if __name__ == "__main__":
    rospy.init_node("tracking_visualization_node", anonymous=True)
    TrackingVisualization()
    try:
        rospy.spin()
    except rospy.ROSInterruptException:
        cv2.destroyAllWindows()

