#!/usr/bin/env python
# coding=utf-8

# TODO: It is generally good form to not do 'from x import y', but to do 'import x' and use it as 'x.y' in the code.


"""
Gets lists of detected objects from Yolo or from the detection_fusion.

Tracks them over time and publishes the results to '/detections_tracking'.
"""
import rclpy
from rclpy.node import Node

import numpy as np

try:
    from Queue import Queue
except ImportError:
    from queue import Queue

from threading import Thread
from tracking.sort import Sort

from msgs.msg import ObjectArray

# Global Queue
msg_queue = Queue(maxsize=15)
publisher = None

class QueueFiller(Node):

    def __init__(self):
        super().__init__('queue_filler')
        global publisher
        self.sub = self.create_subscription(ObjectArray, "yolov5/detections_yolo", self.callback, 10)

        publisher = self.create_publisher(ObjectArray, "tracking/detections_tracking", 1)

    def callback(self, msg):
        global msg_queue
        """
        Put the msg into the msg_queue. If there is already something in the msg_queue, 
        delete it and put the more current msg in there.

        :param msg: Detections from Yolo or the detection_fusion.
        :param msg_queue: The queue into which the msg is to be put. Maxsize=1.
        """
        try:
            msg_queue.put(msg, block=False)
        except:
            msg_queue.get()
            msg_queue.put(msg, block=False)



# TODO: Put iou_calc & find_matching_box into their own file ('bbox_fcts.py' or something like it)
def iou_calc(A, B):
    """
    Calculates how much of the given bboxes overlap as a percentage of 
    the total area that they cover.

    :param A: Bounding box, consisting of [UpperLeft_x, UpperLeft_y, LowerRight_x, LowerRight_y].
    :param B: Bounding box.

    :returns: iou, the intersecting area's part of the total area covered by A and B in percent/100.
    """

    xA = max(A[0], B[0])
    yA = max(A[1], B[1])
    xB = min(A[2], B[2])
    yB = min(A[3], B[3])

    # compute the area of intersection rectangle
    interArea = max(0, xB - xA) * max(0, yB - yA)

    if interArea == 0:
        return 0.0

    # compute the area of both the prediction and ground-truth rectangles
    boxAArea = (A[2] - A[0]) * (A[3] - A[1])
    boxBArea = (B[2] - B[0]) * (B[3] - B[1])

    iou = interArea / float(boxAArea + boxBArea - interArea)
    return iou


def find_matching_box(bbs_1, bb2, match_iou):
    best_iou = match_iou
    best_index = -1
    # TODO: this should be fully parallelizable. Every itereation in the loop is independent from the last. njit it?
    for i in range(len(bbs_1)):
        iou = iou_calc(bbs_1[i].bb, bb2)
        if iou > best_iou:
            best_index = i
            best_iou = iou

    return best_index

 
def convert_to_dets(detected_objects):
    """Convert detected_objects into a form that sort can deal with."""

    if len(detected_objects.objects) > 0:
        # TODO: why is dets an array instead of a np.array? That would remove the need for posprocessing later.
        dets = []
        for detected_obj in detected_objects.objects:
            dets.append([detected_obj.bb[0], 
                         detected_obj.bb[1], 
                         detected_obj.bb[2], 
                         detected_obj.bb[3],
                         detected_obj.confidence])
    else:
        # TODO: why is dets a tuple with one entry, which is a np.array?
        dets = (np.empty((0, 5)))
    return dets
 

def tracking(msg_queue, pub):
    """
    Add an ID to every object in the detections from the msg_queue where this is possible.
    The ID should follow the same object over time, if everything works as it should.

    :param msg_queue: A queue.queue which contains the latest list of detected objects.
    :param pub: The rospy.Publisher which pulishes the results of this function.
    """
    
    mot_tracker = Sort(15, 2, 0.3)
    
    while True: #### improve!!!
        
        detections = msg_queue.get()
        
        # TODO: why is the 'dets = dets[:][:4]' line and the one after it not included in the convert_to_dets function?
        dets = convert_to_dets(detections)
        # Only bounding boxes, no confidence 
        # TODO: Does this even work for Sort.update? Sort.update explicitly asks for [[x1,y1,x2,y2,score],[x1,y1,x2,y2,score],...]
        dets = dets[:][:4]   
        dets = np.array(dets)

        # Assigns IDs to objects that link them over time, from detection to detection.
        trackers = mot_tracker.update(dets)

        # Extract the bounding-boxes from trackers and find the best fit in detections.
        # Mark the respective detections-object with the ID of the trackers-bbox that fits it best.
        for track in trackers:

            # TODO: shouldn't this work using find_matching_box(detections.objects, track[:4], 0.5)? Would avoid a for-loop.
            bb = track[:4]
            best_index = find_matching_box(detections.objects, bb, 0.5)

            if best_index > -1:
                detections.objects[best_index].trackingid = int(track[4])+1
        print('classes: {}, \t\t ids: {}'.format([i.objectclass for i in detections.objects[:]], [i.trackingid for i in detections.objects[:]]))
        pub.publish(detections)


def main(args=None):
    global publisher
    rclpy.init(args=args)

    queue_filler = QueueFiller()

    tracking_thread = Thread(target=tracking, args=(msg_queue, publisher))
    tracking_thread.daemon = True
    tracking_thread.start()
    
    
    print("Detection tracking running")
    
    rclpy.spin(queue_filler)
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    queue_filler.destroy_node()
    tracking_thread.exit()
    rclpy.shutdown()

if __name__ == "__main__":
    main()
    
