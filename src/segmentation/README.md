# Information

This node extract the objects from the point cloud and creates 3D bounding boxes out of the point cloud and YOLO-object-detection informations.

# Usage
Depends on the following nodes to run:
- yolov5_ros
