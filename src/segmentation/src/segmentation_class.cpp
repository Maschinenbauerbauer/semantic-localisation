#include <stdio.h>
#include <math.h>
#include <omp.h>

#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <msgs/msg/object_array.hpp>
#include <msgs/msg/object_information.hpp>

#include <pcl_conversions/pcl_conversions.h> 
#include <pcl/point_cloud.h> 
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/common/io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h> // check A. Bauer
#include <pcl/common/common.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/centroid.h>
#include <pcl/filters/extract_indices.h>

using namespace message_filters;
rclcpp::Publisher<msgs::msg::ObjectArray>::SharedPtr temp_pub;

class Segmentation : public rclcpp::Node
{
  public:
    Segmentation()
    : Node("segmentation"), count_(0)
    {
	sync.registerCallback(boost::bind(this->&callbackMethod, _1, _2));
	}
    void callbackMethod(
	const sensor_msgs::msg::PointCloud2::ConstSharedPtr& cloud_msg, 
	const msgs::msg::ObjectArray::ConstSharedPtr& msg_in)
{
	std::cout<<"PointCloud and ObjectArray received.."<<std::endl;
	float weight_np_score = 0.35;
	float eval_radius_factor = 7;
	float voxel_leaf_size = 0.05;
	float ClusterTolerance = 0.05;
	float MinClusterSize = 3;
	float MaxClusterSize = 2000;

	// Old Code from here:
	msgs::msg::ObjectArray msg = *msg_in;
	msg.header = cloud_msg->header;
	/* Convert ROS message to PCL */
	//pcl::PCLPointCloud2 pcl_pc2;
	//pcl_conversions::toPCL(*cloud_msg,pcl_pc2);

	//pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_pc_in(new pcl::PointCloud<pcl::PointXYZ>);
	//pcl::fromPCLPointCloud2(pcl_pc2,*pcl_pc_in);

	/* Convert ROS message to PCL */
	pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_pc_in(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::fromROSMsg(*cloud_msg, *pcl_pc_in);

	std::cout<<pcl_pc_in->size()<<std::endl;

	//pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_pc_in(new pcl::PointCloud<pcl::PointXYZ>);
	//pcl::fromROSMsg(*cloud_msg, *pcl_pc_in);

	//#pragma omp parallel for       		
	for (size_t i=0; i<msg.objects.size(); ++i)
	{
		msgs::msg::ObjectInformation obj = msg.objects[i];

		int left = obj.bb[0];
		int top = obj.bb[1];
		int right = obj.bb[2];
		int bottom = obj.bb[3];

		std::cout<<"bb: " << obj.bb[0] << ";" << obj.bb[1] << ";"<< obj.bb[2] << ";"<< obj.bb[3] << ";"<<std::endl;

		int eval_radius_l = (int(floor((obj.bb[2] - obj.bb[0])/eval_radius_factor)));
        int eval_radius_t = (int(floor((obj.bb[3] - obj.bb[1])/eval_radius_factor)));
		int center_left = obj.bb[0] + (int(obj.bb[2] - obj.bb[0])) - eval_radius_l;
		int center_right = obj.bb[0] + (int(obj.bb[2] - obj.bb[0])) + eval_radius_l;
		int center_top = obj.bb[1] + (int(obj.bb[3] - obj.bb[1])) - eval_radius_t;
		int center_bottom = obj.bb[1] + (int(obj.bb[3] - obj.bb[1])) + eval_radius_t;

        std::cout<<"c: " << eval_radius_l<< ";" << eval_radius_t<< ";"<< center_left << ";"<< center_top << ";"<< center_bottom << ";" <<std::endl;

		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_object(new pcl::PointCloud<pcl::PointXYZ>);
		
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_obj_center(new pcl::PointCloud<pcl::PointXYZ>);
		
		for(int i=left; i<right; i++)
		{	
			for(int k=top; k<bottom; k++)
			{
				cloud_object->push_back(pcl_pc_in->at(i, k));
				if(i >= center_left && i <= center_right && k >= center_top && k <= center_bottom)
				{
					cloud_obj_center->push_back(pcl_pc_in->at(i, k));
				}
			}
		}	
		std::cout<<"c: " <<cloud_obj_center->size()<<std::endl;
		// maybe push_back does not work? 

		std::cout<<"o: "<<cloud_object->size()<<std::endl;
		
		/* Filter pointcloud voxelbased */
		cloud_object->is_dense = false; // do this for preventing nan errors -> avoided by following function
		cloud_obj_center->is_dense = false;
		
		if(cloud_object->size()<2)
		{
			continue;
		}

		Eigen::Vector4f bb2d_centroid;
		pcl::compute3DCentroid(*cloud_obj_center, bb2d_centroid);

		/* Filter pointcloud voxelbased */
		pcl::VoxelGrid<pcl::PointXYZ> sor;
		sor.setLeafSize (voxel_leaf_size, voxel_leaf_size, voxel_leaf_size);
		sor.setInputCloud(cloud_object);
		sor.filter(*cloud_object);

		pcl::PointXYZ minPt, maxPt;
		pcl::getMinMax3D (*cloud_object, minPt, maxPt);

		float max_dist = sqrt(pow(maxPt.x-minPt.x, 2) +
							pow(maxPt.y-minPt.y, 2) + 
							pow(maxPt.z-minPt.z, 2));

			/* Euclidean clustering */
		pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);

		tree->setInputCloud(cloud_object);

		std::vector<pcl::PointIndices> cluster_indices;

		pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
		ec.setClusterTolerance(ClusterTolerance);
		ec.setMinClusterSize(MinClusterSize);
		ec.setMaxClusterSize(MaxClusterSize);

		ec.setSearchMethod(tree);
		ec.setInputCloud(cloud_object);
		ec.extract(cluster_indices);

		/* Euclidean clustering */
		float cluster_score = 0.0;
		float cloud_obj_size = float(cloud_object->size());
		pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_pc_out(new pcl::PointCloud<pcl::PointXYZ>);

		std::cout<<"d"<<std::endl;
		for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
		{
			pcl::PointCloud<pcl::PointXYZ>::Ptr cluster (new pcl::PointCloud<pcl::PointXYZ>);
			for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
			{
				cluster->push_back((*cloud_object)[*pit]);
			}

			Eigen::Vector4f centroid_cluster;
			pcl::compute3DCentroid(*cluster, centroid_cluster);
			float distance = sqrt(pow(centroid_cluster[0]-bb2d_centroid[0], 2) +
									pow(centroid_cluster[1]-bb2d_centroid[1], 2) + 
								pow(centroid_cluster[2]-bb2d_centroid[2], 2));

			float distance_score = (max_dist-distance) / max_dist;
			float np_score = float(cluster->size()) / cloud_obj_size;
			float current_score = weight_np_score * np_score + (1.0 - weight_np_score) * distance_score;
			if(current_score > cluster_score)
			{
				cluster_score = current_score;
				pcl::copyPointCloud(*cluster, *pcl_pc_out);
			}
		}

		std::cout<<cluster_score<<std::endl;
		if(cluster_score != 0)
		{
			pcl_pc_out->width = pcl_pc_out->size();
			pcl_pc_out->height = 1;
			pcl_pc_out->is_dense = true; 
			
			Eigen::Vector4f centroid;
			pcl::compute3DCentroid(*pcl_pc_out, centroid);
			if(centroid[0] < 5 && centroid[1] < 5 && centroid[2] < 5 &&
					centroid[0] > -5 && centroid[1] > -5 && centroid[2] > -5){
					
				pcl::PointXYZ minPt, maxPt;
				pcl::getMinMax3D(*pcl_pc_out, minPt, maxPt);
				msg.objects[i].bb3d = {minPt.x, minPt.y, minPt.z, maxPt.x, maxPt.y, maxPt.z};

				msg.objects[i].position3d = {centroid[0], centroid[1], centroid[2]};
				
			
				sensor_msgs::msg::PointCloud2 ros_pc_out;
				pcl::toROSMsg(*pcl_pc_out, ros_pc_out);
				ros_pc_out.header.frame_id = msg.header.frame_id;
				ros_pc_out.header.stamp = msg.header.stamp;
				msg.objects[i].pc = ros_pc_out;
			}
		}
	}
	temp_pub->publish(msg);
}


private:
    ros::NodeHandle node;
    ros::Publisher pub;
    std_msgs::Int32 out;
    message_filters::Subscriber<sensor_msgs::msg::PointCloud2> points_sub(node, "/zed2/zed_node/point_cloud/cloud_registered", custom_qos_profile);
  	message_filters::Subscriber<msgs::msg::ObjectArray> object_sub(node, "/detections_yolo", custom_qos_profile);
    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::msg::PointCloud2, msgs::msg::ObjectArray> MySyncPolicy;
    message_filters::Synchronizer<MySyncPolicy> sync(MySyncPolicy(10), points_sub, object_sub);
	// This is how I would define them inside main()
    //Synchronizer<MySyncPolicy> sync(MySyncPolicy(10), sub1, sub2);
    //sync.registerCallback(&callback);
};

int main(int argc, char **argv)
{
	// Create a new Node
	rclcpp::init(argc, argv);
  	//auto node = rclcpp::Node::make_shared("simple_time_sync");
 	temp_pub = node->create_publisher<msgs::msg::ObjectArray>("/segmented_detections", 10);

	// define quality of service: all messages that you want to receive must have the same
	rmw_qos_profile_t custom_qos_profile = rmw_qos_profile_default;

	//message_filters::Subscriber<sensor_msgs::msg::PointCloud2> points_sub(node, "/camera/depth/color/points", custom_qos_profile);
	message_filters::Subscriber<sensor_msgs::msg::PointCloud2> points_sub(node, "/zed2/zed_node/point_cloud/cloud_registered", custom_qos_profile);
  	message_filters::Subscriber<msgs::msg::ObjectArray> object_sub(node, "/detections_yolo", custom_qos_profile);
	
	typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::msg::PointCloud2, msgs::msg::ObjectArray> MySyncPolicy;
	message_filters::Synchronizer<MySyncPolicy> sync(MySyncPolicy(10), points_sub, object_sub);
	
	std::cout<<"Detection segmentation running"<<std::endl;

	rclcpp::spin(node);
	rclcpp::shutdown();
	return 0;
    ros::init(argc, argv, "synchronizer");
    Node synchronizer;

    ros::spin();

}