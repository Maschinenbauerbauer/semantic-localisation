#include <stdio.h>
#include <math.h>
#include <omp.h>
#define THREAD_NUM 4

#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <iterator>

#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <geometry_msgs/msg/point.hpp>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>


#include <msgs/msg/object_array.hpp>
#include <msgs/msg/object_information.hpp>

#include <pcl_conversions/pcl_conversions.h> 
#include <pcl/point_cloud.h> 
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/common/io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/octree/octree.h>
#include <pcl/segmentation/extract_clusters.h> // check A. Bauer
#include <pcl/common/common.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/centroid.h>
#include <pcl/filters/extract_indices.h>

rclcpp::Node::SharedPtr node;
rclcpp::Publisher<msgs::msg::ObjectArray>::SharedPtr temp_pub;
rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr debug_pub;

/*
	// Use this for debugging point clouds in rviz
	sensor_msgs::msg::PointCloud2 pcl_ros;
	pcl::toROSMsg(*cloud_object, pcl_ros);
	pcl_ros.header.frame_id = "zed2_left_camera_frame";
	rclcpp::Time now = node->get_clock()->now();
	pcl_ros.header.stamp = now;
	debug_pub->publish(pcl_ros);
	//std::cin.ignore(); // Halts the program until enter is hit
*/

bool inRange(unsigned low, unsigned high, unsigned x)
{
    return  ((x-low) <= (high-low));
}

std::vector<geometry_msgs::msg::Point> generate_point_array(float xmin, float ymin, float zmin, float xmax, float ymax, float zmax) {
	// Shape of skeleton:
	//    7 -------- 6
	//   /|         /|
	//  / |        / |
	// 4 -------- 5  |
	// |  |       |  |
	// |  3 ------|- 2        z
	// | /        | /         |  y
	// |/         |/          | /
	// 0 -------- 1     	  |/____ x
	
	geometry_msgs::msg::Point p0;
	p0.x = xmin; p0.y = ymin; p0.z = zmin;
	geometry_msgs::msg::Point p1;
	p1.x = xmax; p1.y = ymin; p1.z = zmin;
	geometry_msgs::msg::Point p2;
	p2.x = xmax; p2.y = ymax; p2.z = zmin;
	geometry_msgs::msg::Point p3;
	p3.x = xmin; p3.y = ymax; p3.z = zmin;
	geometry_msgs::msg::Point p4;
	p4.x = xmin; p4.y = ymin; p4.z = zmax;
	geometry_msgs::msg::Point p5;
	p5.x = xmax; p5.y = ymin; p5.z = zmax;
	geometry_msgs::msg::Point p6;
	p6.x = xmax; p6.y = ymax; p6.z = zmax;
	geometry_msgs::msg::Point p7;
	p7.x = xmin; p7.y = ymax; p7.z = zmax;
	
	std::vector<geometry_msgs::msg::Point> vec;
	vec.push_back(p0);
	vec.push_back(p1);
	vec.push_back(p2);
	vec.push_back(p3);
	vec.push_back(p4);
	vec.push_back(p5);
	vec.push_back(p6);
	vec.push_back(p7);
	return vec;
}


void callbackMethod(
	const sensor_msgs::msg::PointCloud2::ConstSharedPtr& cloud_msg, 
	const msgs::msg::ObjectArray::ConstSharedPtr& msg_in)
{
	std::cout<<"PointCloud and ObjectArray received.."<<std::endl;
	double weight_np_score = node->get_parameter("weight_np_score").as_double(); 
	double eval_radius_factor = node->get_parameter("eval_radius_factor").as_double(); 
	double voxel_leaf_size = node->get_parameter("voxel_leaf_size").as_double();
	double ClusterTolerance = node->get_parameter("ClusterTolerance").as_double();
	double MinClusterSize = node->get_parameter("MinClusterSize").as_double();
	double MaxClusterSize = node->get_parameter("MaxClusterSize").as_double();

	//std::cout << "weight_np_score: " << weight_np_score << "eval_radius_factor: " << eval_radius_factor;

	// Old Code from here:
	msgs::msg::ObjectArray msg = *msg_in;
	msg.header = cloud_msg->header;
	/* Convert ROS message to PCL */
	//pcl::PCLPointCloud2 pcl_pc2;
	//pcl_conversions::toPCL(*cloud_msg,pcl_pc2);

	//pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_pc_in(new pcl::PointCloud<pcl::PointXYZ>);
	//pcl::fromPCLPointCloud2(pcl_pc2,*pcl_pc_in);

	/* Convert ROS message to PCL */
	pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_pc_in(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::fromROSMsg(*cloud_msg, *pcl_pc_in);

	//std::cout<<pcl_pc_in->size()<<std::endl;

	//pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_pc_in(new pcl::PointCloud<pcl::PointXYZ>);
	//pcl::fromROSMsg(*cloud_msg, *pcl_pc_in);

	try
	{
		/*
		if (pcl_pc_in->height == 1) 
		{
			std::cout << "PointCloud unordered -> Reorganizing" << "\n" << std::endl;
			//pcl_pc_in->height = 720;
			//pcl_pc_in->width = 1080;
			pcl::types::unindex_t width = 1080;
			pcl::types::unindex_t height = 720;
			pcl_pc_in->resize(width, height);
		}
		*/

		for (size_t i=0; i<msg.objects.size(); ++i)
		{
			msgs::msg::ObjectInformation obj = msg.objects[i];


			// The objects bounding boxes
			// Remember: These are pixel coordinates where the origin is in the top left corner. x to the right, y to the bottom.
			int left = obj.bb[0];
			int top = obj.bb[1];
			int right = obj.bb[2];
			int bottom = obj.bb[3];

			std::cout<<"bb: " << obj.bb[0] << ";" << obj.bb[1] << ";"<< obj.bb[2] << ";"<< obj.bb[3] << ";"<<std::endl;

			// Evaluation window für Euclidian Clustering
			int eval_radius_l = int((obj.bb[2] - obj.bb[0]) * eval_radius_factor);
			int eval_radius_t = int((obj.bb[3] - obj.bb[1]) * eval_radius_factor);

			// Center Point for Centroid Calculation:
			int center_left = obj.bb[0] + int((obj.bb[2] - obj.bb[0])/2) - eval_radius_l;
			int center_right = obj.bb[0] + int((obj.bb[2] - obj.bb[0])/2) + eval_radius_l;
			int center_top = obj.bb[1] + int((obj.bb[3] - obj.bb[1])/2) - eval_radius_t;
			int center_bottom = obj.bb[1] + int((obj.bb[3] - obj.bb[1])/2) + eval_radius_t;

			std::cout<<"c: " << eval_radius_l<< ";" << eval_radius_t<< ";"<< center_left << ";"<< center_top << ";"<< center_bottom << ";" <<std::endl;

			// All Points of global point cloud inside yolo bounding box
			
			pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_object(new pcl::PointCloud<pcl::PointXYZ>);
			
			pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_obj_center(new pcl::PointCloud<pcl::PointXYZ>);

			//std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

			//cloud_object->reserve(MaxClusterSize);
			//cloud_obj_center->reserve(MaxClusterSize);

			for(int i=left; i<right; i++)
			{
				for(int k=top; k<bottom; k++)
				{
					cloud_object->push_back(pcl_pc_in->at(i, k));
					//if(i >= center_left && i <= center_right && k >= center_top && k <= center_bottom)
					if (inRange(center_left, center_right, i) && inRange(center_top, center_bottom, k))
					{
						cloud_obj_center->push_back(pcl_pc_in->at(i, k));
					}
				}
			}	

			// Use this for debugging point clouds in rviz
			sensor_msgs::msg::PointCloud2 pcl_ros;
			pcl::toROSMsg(*cloud_obj_center, pcl_ros);
			pcl_ros.header.frame_id = "zed2_left_camera_frame";
			rclcpp::Time now = node->get_clock()->now();
			pcl_ros.header.stamp = now;
			debug_pub->publish(pcl_ros);
			//std::cin.ignore(); // Halts the program until enter is hit

			//std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
			//std::cout << "a Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;
			//std::cout << "a Time difference = " << std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count() << "[ns]" << std::endl;

			/* Filter pointcloud voxelbased */
			cloud_object->is_dense = false; // do this for preventing nan errors -> avoided by following function
			cloud_obj_center->is_dense = false;
			
			if(cloud_object->size()<10)
			{
				std::cout << "Point cloud_object size too small" << std::endl;
				continue;
			}

			Eigen::Vector4f bb2d_centroid;
			pcl::compute3DCentroid(*cloud_obj_center, bb2d_centroid);

			/* Filter pointcloud voxelbased */
			pcl::VoxelGrid<pcl::PointXYZ> sor;
			sor.setLeafSize (voxel_leaf_size, voxel_leaf_size, voxel_leaf_size);
			sor.setInputCloud(cloud_object);
			sor.filter(*cloud_object);

			pcl::PointXYZ minPt, maxPt;
			pcl::getMinMax3D (*cloud_object, minPt, maxPt);
			//std::cout << "minPt: " << minPt << "maxPt: " << maxPt << std::endl;

			float max_dist = sqrt(pow(maxPt.x-minPt.x, 2) +
								pow(maxPt.y-minPt.y, 2) + 
								pow(maxPt.z-minPt.z, 2));

				/* Euclidean clustering */
			pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
			tree->setInputCloud(cloud_object);

			std::vector<pcl::PointIndices> cluster_indices;
			std::cout << "a" << "\n" << std::endl;

			pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
			ec.setClusterTolerance(ClusterTolerance);
			ec.setMinClusterSize(MinClusterSize);
			ec.setMaxClusterSize(MaxClusterSize);

			ec.setSearchMethod(tree);
			ec.setInputCloud(cloud_object);
			ec.extract(cluster_indices);

			std::cout << "b" << "\n" << std::endl;

			/* Euclidean clustering */
			float cluster_score = 0.0;
			float cloud_obj_size = float(cloud_object->size());
			pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_pc_out(new pcl::PointCloud<pcl::PointXYZ>);

			//begin = std::chrono::steady_clock::now();

			for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
			{
				pcl::PointCloud<pcl::PointXYZ>::Ptr cluster (new pcl::PointCloud<pcl::PointXYZ>);
				for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
				{
					// what does this do? Does it only copy the point cloud? There must be faster methods!
					cluster->push_back((*cloud_object)[*pit]);
				}

				Eigen::Vector4f centroid_cluster;
				pcl::compute3DCentroid(*cluster, centroid_cluster);
				//std::cout << "c" << "\n" << std::endl;
				float distance = sqrt(pow(centroid_cluster[0]-bb2d_centroid[0], 2) +
										pow(centroid_cluster[1]-bb2d_centroid[1], 2) + 
									pow(centroid_cluster[2]-bb2d_centroid[2], 2));

				float distance_score = (max_dist-distance) / max_dist; // how important is the distance between the centroids
				float np_score = float(cluster->size()) / cloud_obj_size; // how important is a big cluster
				float current_score = weight_np_score * np_score + (1.0 - weight_np_score) * distance_score;
				if(current_score > cluster_score)
				{
					cluster_score = current_score;
					pcl::copyPointCloud(*cluster, *pcl_pc_out);
					//std::cout << "d" << "\n" << std::endl;
				}
			}

			//end = std::chrono::steady_clock::now();
			//std::cout << "b Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;
			

			//std::cout<<cluster_score<<std::endl;
			if(cluster_score != 0)
			{
				pcl_pc_out->width = pcl_pc_out->size();
				pcl_pc_out->height = 1;
				pcl_pc_out->is_dense = true; 
				
				Eigen::Vector4f centroid;
				pcl::compute3DCentroid(*pcl_pc_out, centroid);
				if(centroid[0] < 5 && centroid[1] < 5 && centroid[2] < 5 &&
						centroid[0] > -5 && centroid[1] > -5 && centroid[2] > -5){
						
					pcl::PointXYZ minPt, maxPt;
					pcl::getMinMax3D(*pcl_pc_out, minPt, maxPt);

					std::vector<geometry_msgs::msg::Point> bb3d = generate_point_array(minPt.x, minPt.y, minPt.z, maxPt.x, maxPt.y, maxPt.z);
					
					msg.objects[i].bb3d = bb3d;

					msg.objects[i].position3d = {centroid[0], centroid[1], centroid[2]};
								
					sensor_msgs::msg::PointCloud2 ros_pc_out;
					pcl::toROSMsg(*pcl_pc_out, ros_pc_out);
					ros_pc_out.header.frame_id = msg.header.frame_id;
					ros_pc_out.header.stamp = msg.header.stamp;
					msg.objects[i].pc = ros_pc_out;
				}
			}
				
		}
		temp_pub->publish(msg);
	}
	catch (...) 
	{
		std::cout << "An exception occured. Exception Nr. " << "\n" << std::endl;
	}
}

int main (int argc, char** argv)
{
	// Create a new Node
	rclcpp::init(argc, argv);
  	node = rclcpp::Node::make_shared("segmentation");
 	temp_pub = node->create_publisher<msgs::msg::ObjectArray>("segmentation/segmented_detections", 10);
	debug_pub = node->create_publisher<sensor_msgs::msg::PointCloud2>("segmentation/debug", 10);
	node->declare_parameter<double>("weight_np_score", 0.4); // defines, wether point cloud size or centroid distance is more important
	node->declare_parameter<double>("eval_radius_factor", 0.3); // how many percent are used for centroid calc (between 0.0 and 1.0)
	node->declare_parameter<double>("voxel_leaf_size", 0.025);
	node->declare_parameter<double>("ClusterTolerance", 0.03);
	node->declare_parameter<double>("MinClusterSize", 5.0);
	node->declare_parameter<double>("MaxClusterSize", 12000.0);

	// define quality of service: all messages that you want to receive must have the same
	rmw_qos_profile_t custom_qos_profile = rmw_qos_profile_default;

	//message_filters::Subscriber<sensor_msgs::msg::PointCloud2> points_sub(node, "/camera/depth/color/points", custom_qos_profile);
	message_filters::Subscriber<sensor_msgs::msg::PointCloud2> points_sub(node, "/zed2/zed_node/point_cloud/cloud_registered", custom_qos_profile);
  	message_filters::Subscriber<msgs::msg::ObjectArray> object_sub(node, "tracking/detections_tracking", custom_qos_profile);
	
	typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::msg::PointCloud2, msgs::msg::ObjectArray> MySyncPolicy;
	message_filters::Synchronizer<MySyncPolicy> sync(MySyncPolicy(1), points_sub, object_sub);
	sync.registerCallback(boost::bind(&callbackMethod, _1, _2));
	std::cout<<"Detection segmentation running"<<std::endl;

	rclcpp::spin(node);
	rclcpp::shutdown();
	return 0;
}