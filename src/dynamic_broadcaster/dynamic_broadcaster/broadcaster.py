import rclpy
import sys

from geometry_msgs.msg import TransformStamped
from rclpy.node import Node
from scipy.spatial.transform import Rotation as R
from tf2_ros.transform_broadcaster import TransformBroadcaster
from geometry_msgs.msg import PoseStamped

class DynamicBroadcaster(Node):

    def __init__(self):
        super().__init__('dynamic_broadcaster')
        self.tfb_ = TransformBroadcaster(self)
        self.sub_pose = self.create_subscription(PoseStamped, "zed2/zed_node/pose", self.handle_pose, 1)

    def handle_pose(self, msg):
        
        tfs = TransformStamped()
        tfs.header.stamp = msg.header.stamp
        tfs.header.frame_id="map"
        tfs._child_frame_id = 'zed2_left_camera_frame'
        tfs.transform.translation.x = msg.pose.position.x
        tfs.transform.translation.y = msg.pose.position.y
        tfs.transform.translation.z = msg.pose.position.z

        self.get_logger().info('Got transform {}-{}-{} (x, y, z)'.format(msg.pose.position.x, msg.pose.position.y, msg.pose.position.z))

        tfs.transform.rotation.x = msg.pose.orientation.x
        tfs.transform.rotation.y = msg.pose.orientation.y
        tfs.transform.rotation.z = msg.pose.orientation.z
        tfs.transform.rotation.w = msg.pose.orientation.w

        self.tfb_.sendTransform(tfs)    

def main():
    rclpy.init()
    node = DynamicBroadcaster()

    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass

    node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()