# Information

A C++ Node that detects a moving object (humans) in the point cloud and publishes their location as well as information about the object. 
This is used in the segmentation/mapping stage to keep an information about the possible moving object in the surrounding.

Running this is not mandatory!
