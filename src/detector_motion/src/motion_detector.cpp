#include <opencv2/opencv.hpp>

#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/image.hpp"

#include "cv_bridge/cv_bridge.h"

#include <iostream>
#include <cstdlib>
#include <algorithm>

#include <chrono>

using namespace std::placeholders;

class MotionDetector : public rclcpp::Node
{
    public:
        MotionDetector(): Node("motion_detector")
        {
            subscription_ = this->create_subscription<sensor_msgs::msg::Image>("camera/color/image_raw", 10, std::bind(&MotionDetector::callback, this, _1));
            //publisher_ = this->create_publisher<sensor_msgs::msg::Image>("new_image", 10);
            
            img_0_set = false;
            img_1_set = false;
            img_2_set = false;
        }
        
        ~MotionDetector()
        {
            cv::destroyAllWindows();
        }
        
    private:
        
        rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr subscription_;  
        
        cv_bridge::CvImagePtr cv_ptr;
        
        cv::Mat img_0;
        cv::Mat img_1;
        cv::Mat img_2;
        
        cv::Mat img_0_c;
        cv::Mat img_1_c;
        cv::Mat img_2_c;
        
        double stamp_img_0;
        double stamp_img_1;
        double stamp_img_2;
        
        bool img_0_set;
        bool img_1_set;
        bool img_2_set;
        
        std::vector<cv::Point2f> p_r;
        
        cv::TermCriteria criteria = cv::TermCriteria((cv::TermCriteria::COUNT) + (cv::TermCriteria::EPS), 10, 0.03);
        
        //rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr publisher_;
        //std::cout <<"\n New Image" << std::endl;
        
        void callback(const sensor_msgs::msg::Image::SharedPtr msg)
        {
            this->cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
            
            if(img_0_set == false)
            {
                this->img_0_c = this->cv_ptr->image;
                this->stamp_img_0 = msg->header.stamp.nanosec;
                cv::cvtColor(this->img_0_c, this->img_0, cv::COLOR_BGR2GRAY);
                img_0_set = true;
            }
            else if(img_1_set == false)
            {
                this->img_1_c = this->cv_ptr->image;
                this->stamp_img_1 = msg->header.stamp.nanosec;
                cv::cvtColor(this->img_1_c, this->img_1, cv::COLOR_BGR2GRAY);
                img_1_set = true;
            }
            else
            {
                this->img_2_c = this->cv_ptr->image;
                this->stamp_img_2 = msg->header.stamp.nanosec;
                img_2_set = true;
                
                double t1 = (stamp_img_1 - stamp_img_0)/1000000;
                double t2 = (stamp_img_2 - stamp_img_1)/1000000;

                if (t1 < 40 && t2 < 40)
                {
                    
                    cv::cvtColor(this->img_2_c, this->img_2, cv::COLOR_BGR2GRAY);
                    
                    cv::goodFeaturesToTrack(this->img_0, this->p_r, 200, 0.1, 5, cv::Mat(), 3, false, 0.04);

                    cv::Mat img_a_registered;
                    cv::Mat img_b_registered;
                    
                    register_images(this->img_1, this->img_0, this->p_r, img_a_registered);
                    register_images(this->img_1, this->img_2, this->p_r, img_b_registered);
                    
                    if(img_a_registered.empty() == false && img_b_registered.empty() == false)
                    {
                        cv::Mat diff_a;
                        cv::Mat diff_b;
                        cv::Mat diff_c;
                        cv::Mat diff_1;
                        cv::Mat diff_2;

                        cv::absdiff(this->img_1, img_a_registered, diff_a);
                        cv::absdiff(this->img_1, img_b_registered, diff_b);
                        cv::absdiff(img_b_registered, img_a_registered, diff_c);
                        
                        cv::threshold(diff_a, diff_a, 60, 255, cv::THRESH_BINARY);
                        cv::threshold(diff_b, diff_b, 60, 255, cv::THRESH_BINARY);
                        cv::threshold(diff_c, diff_c, 60, 255, cv::THRESH_BINARY);
  
                        cv::Mat element = getStructuringElement(cv::MORPH_RECT, cv::Size(4, 4));
                        cv::morphologyEx(diff_a, diff_a, cv::MORPH_DILATE, element);
                        cv::morphologyEx(diff_b, diff_b, cv::MORPH_DILATE, element);
                        
                        cv::bitwise_and(diff_c, diff_a, diff_1);
                        cv::bitwise_and(diff_1, diff_b, diff_2);
                        
                        cv::Mat element2 = getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
                        cv::morphologyEx(diff_2, diff_2, cv::MORPH_OPEN, element2);
                        cv::Mat element4 = getStructuringElement(cv::MORPH_RECT, cv::Size(4, 4));
                        cv::morphologyEx(diff_2, diff_2, cv::MORPH_CLOSE, element4);
                        cv::Mat element3 = getStructuringElement(cv::MORPH_RECT, cv::Size(8, 8));
                        cv::morphologyEx(diff_2, diff_2, cv::MORPH_DILATE, element3);
                        
                        //cv::compare(diff_c, diff_res1, diff_res2, cv::CMP_NE);
                        
                        cv::imshow("1", diff_2);
                        
                        /*
                        cv::Mat new_image = cv::Mat::zeros(cv::Size(diff_a.cols, diff_a.rows), CV_8UC3);
                        for(int y = 0; y < diff_a.rows; y++) 
                        {
                            for(int x = 0; x < diff_a.cols; x++) 
                            {
                                if(diff_a.at<uchar>(y,x) != 0)
                                {
                                    new_image.at<cv::Vec3b>(y,x)[0] = 255;
                                }
                            }
                        }
                        
                        for(int y = 0; y < diff_b.rows; y++) 
                        {
                            for(int x = 0; x < diff_b.cols; x++) 
                            {
                                if(diff_b.at<uchar>(y,x) != 0)
                                {
                                    new_image.at<cv::Vec3b>(y,x)[2] = 255;
                                }
                            }
                        }
                        
                        for(int y = 0; y < diff_c.rows; y++) 
                        {
                            for(int x = 0; x < diff_c.cols; x++) 
                            {
                                if(diff_c.at<uchar>(y,x) != 0)
                                {
                                    new_image.at<cv::Vec3b>(y,x)[1] = 255;
                                }
                            }
                        }
                        cv::imshow("C", new_image);
                        */
                        
                        std::vector<std::vector<cv::Point> > contours;
                        std::vector<cv::Vec4i> hierarchy;
                        cv::findContours(diff_2, contours, hierarchy,  cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

                        std::vector<std::vector<cv::Point>> contours_poly( contours.size());
                        
                        cv::Scalar color = cv::Scalar(0, 255, 0);
                        
                        for( size_t i = 0; i<contours.size(); i++ )
                        {
                            cv::approxPolyDP(contours[i], contours_poly[i], 3, true);
                            cv::Rect boundRect = cv::boundingRect(contours_poly[i]);
                            if ( ( boundRect.height * boundRect.width ) > 200 )
                            {
                                int tl_x = boundRect.x;
                                int tl_y = boundRect.y;
                                int br_x = (boundRect.x + boundRect.width);
                                int br_y = (boundRect.y + boundRect.height);
                                
                                double f_row = diff_2.rows/5;
                                double f_col = diff_2.cols/5;
                                 
                                //if( ( tl_x > f_col ) && ( tl_y > f_row ) 
                                //    && ( br_x < (diff_2.cols-f_col) ) && ( br_y < (diff_2.rows-f_row) ) )
                                //{
                                cv::rectangle(img_0_c, boundRect.tl(), boundRect.br(), color, 2);
                                //}
                            }
                        }
                        cv::imshow("R", img_0_c);
                    }
                    
                    cv::waitKey(1);  

                    this->img_0 = this->img_1.clone();
                    this->img_1 = this->img_2.clone();
                    
                    this->img_0_c = this->img_1_c.clone();
                    this->img_1_c = this->img_2_c.clone();

                    this->stamp_img_0 = this->stamp_img_1;
                    this->stamp_img_1 = this->stamp_img_2;
                    
                    this->p_r.clear();
                }
                else
                {
                    img_0_set = false;
                    img_1_set = false;
                }
            }
        }
        
        void register_images(cv::Mat& img_1, cv::Mat& img_2, std::vector<cv::Point2f>& p_1, cv::Mat& img_ret)
        {
            std::vector<uchar> status;
            std::vector<float> err;
            
            std::vector<cv::Point2f> p_2;
            std::vector<cv::Point2f> p_1_r;
            
            std::vector<cv::Point2f> p_1_good;
            std::vector<cv::Point2f> p_2_good;

            cv::calcOpticalFlowPyrLK(img_1, img_2, p_1, p_2, status, err, cv::Size(8,8), 3, this->criteria);
            cv::calcOpticalFlowPyrLK(img_2, img_1, p_2, p_1_r, status, err, cv::Size(8,8), 3, this->criteria);    
            
            for(size_t i = 0; i < p_1.size(); i++)
            { 
                float d = std::sqrt(std::pow((p_1[i].x-p_1_r[i].x),2)+std::pow((p_1[i].y-p_1_r[i].y),2));
                if(d < 1.0){
                    p_1_good.push_back(p_1[i]);
                    p_2_good.push_back(p_2[i]);
                }
            }

            if(p_1_good.size() > 10 && p_2_good.size() > 10)
            {
                cv::Mat M = cv::findHomography(p_1_good, p_2_good, cv::RANSAC, 0.5);
                
                if(M.empty() == false)
                {
                    cv::warpPerspective(img_2, img_ret, M, img_2.size(), cv::INTER_LINEAR + cv::WARP_INVERSE_MAP);
                }
            }
            return;
        }
        
        //rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr publisher_;
    };

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<MotionDetector>());
  rclcpp::shutdown();
  return 0;
}

                           /*int tl_x = boundRect.x;
                            int tl_y = boundRect.y;
                            int br_x = (boundRect.x + boundRect.width);
                            int br_y = (boundRect.y + boundRect.height);
                            
                            if(tl_x < 0){tl_x = 0;}
                            if(tl_y < 0){tl_y = 0;}
                            if(br_x < 0){br_x = 0;}
                            if(br_y < 0){br_y = 0;}

                            if(tl_x > 640){tl_x = 640;}
                            if(tl_y > 480){tl_y = 480;}
                            if(br_x > 640){br_x = 640;}
                            if(br_y > 480){br_y = 480;}

                            perception_msgs::object_information object;
                            object.semantics.push_back("dynamic");
                            object.confidence = 0.1;
                            object.boundingbox[0] = tl_x;
                            object.boundingbox[1] = tl_y;
                            object.boundingbox[2] = br_x;
                            object.boundingbox[3] = br_y;
                            recognized_objects.objects.push_back(object);*/
