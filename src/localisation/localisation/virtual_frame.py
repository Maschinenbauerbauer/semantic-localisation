from geometry_msgs.msg import TransformStamped

import rclpy
from rclpy.node import Node

from tf2_ros import TransformBroadcaster


class VirtualFramePublisher(Node):
   """
   This creates a tf2 frame which can be manipulated via a subcription.
   Just pass a TransformStamped to the self.updater topic and the Node will
   update the Transformation.
   """

   def __init__(self):
      super().__init__('virtual_frame_tf2_broadcaster')

      self.br = TransformBroadcaster(self) 

      self.trans = None
      
      self.updater = self.create_subscription(
            TransformStamped,
            'localisation/transform_params',
            self.update_frame,
            1
        )

      self.timer = self.create_timer(0.1, self.broadcast_timer_callback)


   def update_frame(self, msg):
      self.trans = msg


   def broadcast_timer_callback(self):
      t = self.trans
      if t is not None:
         t.header.stamp = self.get_clock().now().to_msg()
         #self.get_logger().info('Broadcasting virtual_frame.')
         self.br.sendTransform(t)


def main():
   rclpy.init()
   node = VirtualFramePublisher()
   try:
      rclpy.spin(node)
   except KeyboardInterrupt:
      pass

   rclpy.shutdown()