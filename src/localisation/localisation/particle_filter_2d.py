# Tutorial from: https://github.com/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/12-Particle-Filters.ipynb
import rclpy
from rclpy.node import Node
from random import random
import numpy as np
from numpy.linalg import norm
from numpy.random import uniform, randn
from filterpy.monte_carlo import systematic_resample
import scipy
from scipy import stats
from msgs.msg import DatabaseMsg, ObjectArray
from pytorch3d.ops import box3d_overlap
import torch

import cv2
cv2_debugging = True

def create_uniform_particles(x_range, y_range, hdg_range, N):
    # We maintain an x, y and heading of the robot. Heading only accounts for an angle. N=Number of particles.
    particles = np.empty((N, 3))
    particles[:, 0] = uniform(x_range[0], x_range[1], size=N)
    particles[:, 1] = uniform(y_range[0], y_range[1], size=N)
    particles[:, 2] = uniform(hdg_range[0], hdg_range[1], size=N)
    particles[:, 2] %= 2 * np.pi
    return particles


def create_gaussian_particles(mean, std, N):
    # This can be used, if the initial position of the robot is known.
    particles = np.empty((N, 3))
    particles[:, 0] = mean[0] + (randn(N) * std[0])
    particles[:, 1] = mean[1] + (randn(N) * std[1])
    particles[:, 2] = mean[2] + (randn(N) * std[2])
    particles[:, 2] %= 2 * np.pi
    return particles


def predict(particles, u, std, dt=1.):
    """ move according to control input u (heading change, velocity)
    with noise Q (std heading change, std velocity)`"""

    # we need to add noise, because the movement is not 100% accurate!

    N = len(particles)
    # update heading
    particles[:, 2] += u[0] + (randn(N) * std[0])
    particles[:, 2] %= 2 * np.pi

    # move in the (noisy) commanded direction
    dist = (u[1] * dt) + (randn(N) * std[1]) # distance of the noisy movement vector
    particles[:, 0] += np.cos(particles[:, 2]) * dist
    particles[:, 1] += np.sin(particles[:, 2]) * dist


def update(particles, weights, z, R, landmarks):
    # The update step calculates (=updates) the weights of the current particles
    landmarks = landmarks[:, 0:2].astype(np.float32)
    for i, landmark in enumerate(landmarks):
        # Step 1: Calculate the distance of each particle to the landmarks
        distance = np.linalg.norm(particles[:, 0:2] - landmark, axis=1)
        #print('Distance:  {}'.format(distance))
        # Step 2: Set the weight according to the overall mean distance to the landmarks times a noise factor R
        weights *= scipy.stats.norm(distance, R).pdf(z[i])

    weights += 1.e-300      # avoid round-off to zero
    weights /= sum(weights) # normalize


def estimate(particles, weights):
    """returns mean and variance of the weighted particles"""
    # Out of the updated particles, calculate an estimated, most likely position

    pos = particles[:, 0:2] # all positions (slice of the first two rows)
    # for np.average see https://numpy.org/doc/stable/reference/generated/numpy.average.html
    mean = np.average(pos, weights=weights, axis=0) # calculate the mean of the position but with the updated weight! 
    var  = np.average((pos - mean)**2, weights=weights, axis=0) # calculate the variance but with the updated weight! 
    return mean, var


def resample_from_index(particles, weights, indexes):
    # This implements a Sampling Importance Resampling filter short SIR-Filter
    particles[:] = particles[indexes]
    weights.resize(len(particles))
    weights.fill (1.0 / len(weights))
    return weights


def neff(weights):
    return 1. / np.sum(np.square(weights))


class ParticleFilterEstimator(Node):

    def __init__(self):
        super().__init__('particle_filter_estimator')

        self.initial_position_known = False

        self.count = 0
        timer_period = .05  # seconds
        self.timer = self.create_timer(timer_period, self.particle_filter_iteration)
        
        self.landmark_subscription = self.create_subscription(
            DatabaseMsg,
            'mapping/current_db',
            self.landmark_update,
            1
        )

        self.visible_objects_subscription = self.create_subscription(
            ObjectArray,
            'segmentation/segmented_detections',
            self.visible_objects_update,
            1
        )

        self.N = 500
        self.sensor_std_err = .5

        self.robot_pos = np.array([0.0, 0.0, 0.0]) # odometry position in map coordinates

        self.landmarks = None # The landmarks from the map database
        self.visible_objects = [] # The visible objects from the cameras fov
        self.xs_history = []
        self.particles = []
        self.last_pos_known_to_pf = self.robot_pos # initial
        self.weights = np.ones(self.N) / self.N
        self.fusion_radius = 0.5

    def landmark_update(self, database):
        # for now, we only use the x and y position and the objectclass of the landmarks
        landmarks = []
        for item in database.objects:
            landmarks.append([float(item.position[0]), float(item.position[1]), float(item.position[2]), item.objectclass, item.dbid, item.bb3d])

        #self.get_logger().info('Landmarks: {}'.format(landmarks))
        self.landmarks = np.array(landmarks)
        pass
    
    def visible_objects_update(self, msg):
        # Update the visible objects
        self.visible_objects = msg.objects
        pass

    def position_update(self, msg):
        # Update the last odometry position here via a subscription
        pass

    def particle_filter_iteration(self):
        img = np.zeros((720, 1280, 3))
        # Run the particle filter algorithm

        if self.landmarks is not None: # If the landmarks were updated at least once!  
            self.NL = len(self.landmarks)

            if self.count == 0:
                # create particles and weights
                if self.initial_position_known:
                    self.particles = create_gaussian_particles(mean=self.initial_x, std=(5, 5, np.pi/4), N=self.N)
                else:
                    #self.get_logger().info('Landmarks: {}'.format(self.landmarks))
                    expansion_factor = 2.0
                    self.particles = create_uniform_particles(
                        x_range=(-np.amax(self.landmarks[:,0].astype(np.float32))*expansion_factor, np.amax(self.landmarks[:,0].astype(np.float32))*expansion_factor),
                        y_range=(-np.amax(self.landmarks[:,1].astype(np.float32))*expansion_factor, np.amax(self.landmarks[:,1].astype(np.float32))*expansion_factor),
                        hdg_range=(0,0), # for now, no speed is accounted for
                        N=self.N
                    )

            #self.robot_pos = self.last_pos
            # Movement from last known position
            #movement = norm(self.last_pos_known_to_pf, axis=1) # for now, not in use

            # Measured real distance from robot to each landmark
            """
            Hier müssen wir eigentlich die Distanzen zu allen Objekten in der Umgebung berechnen.
            Das ist allerdings nicht möglich, da unser FOV die sichtbaren Objekte einschränkt.
            Was also tun wir mit den Objekten, die wir nicht eindeutig zuordnen können?
            Für den Anfang setzen wir deren Distanzen auf Unendlich.
            Da wir auch keine Instanzierung durchführen können, müssen wir alle gleichen Objekte als Möglichkeit heranziehen.
            Dafür setzen wir für alle Objekte mit der selben Objectclass den selben Distanzwert.
            """
            measured_real_distances = self.get_seen_distances(self.landmarks, self.visible_objects) + (randn(self.NL) * self.sensor_std_err)
            #self.get_logger().info('Distances {}'.format(self.landmarks[:,0].astype(np.float32)))

            predict(self.particles, u=(0.0, 0.0), std=(.2, .05))
            
            # incorporate measurements
            #self.get_logger().info('Real distances {}'.format(measured_real_distances))
            #self.get_logger().info('Particles {}'.format(self.particles))
            update(self.particles, self.weights, z=measured_real_distances, R=self.sensor_std_err, 
                landmarks=self.landmarks)
            
            #self.get_logger().info('Weights:\n{}'.format(sorted(self.weights, reverse=True)))
            #self.get_logger().info('Particles:\n{}'.format(self.particles))
            
            # resample if too few effective particles
            if neff(self.weights) < self.N/2:
                indexes = systematic_resample(self.weights)
                self.weights = resample_from_index(self.particles, self.weights, indexes)
                assert np.allclose(self.weights, 1/self.N)
            mu, var = estimate(self.particles, self.weights)
            self.xs_history.append(mu)
                
            #self.get_logger().info(str(mu))

            if cv2_debugging:
                scale = 100
                for _x,_y,_ in self.particles:
                    x, y = int(_x*scale+1280/2), int(_y*scale+720/2)
                    #self.get_logger().info('x {}, y: {}'.format(x, y))
                    cv2.circle(img, (x,y), radius=1, color=(0, 0, 255), thickness=2)

                for l in self.landmarks[:,0:2].astype(np.float32):
                    x, y = int(l[0]*scale+1280/2), int(l[1]*scale+720/2)
                    #self.get_logger().info('x {}, y: {}'.format(x, y))
                    cv2.circle(img, (x,y), radius=5, color=(0, 255, 0), thickness=2)

                cv2.imshow('Particle Filter', img)
                cv2.waitKey(1)
            
            self.count += 1
        pass

    def get_seen_distances(self, landmarks, visible_objects):
        ret = np.array([np.inf]*len(landmarks))
        
        """

        unique_visible_object_class_names = []

        for item in visible_objects:
            if list(map(lambda x: x.objectclass, visible_objects)).count(item.objectclass) == 1:
                unique_visible_object_class_names.append(item.objectclass)

        self.get_logger().info('Unique Objects:  {}'.format(unique_visible_object_class_names))
        
        for visibleObj in visible_objects:
            visible_objectclass = visibleObj.objectclass
            #self.get_logger().info('Visible Object:  {}'.format(visible_objectclass))
            if visible_objectclass in unique_visible_object_class_names: # Wenn eindeutige Instanz
                
                visible_distance = np.linalg.norm(visibleObj.position3d[0:2]) # erstmal nur x und y

                for i, marker in enumerate(landmarks):
                    # Marker related variables
                    marker_objectclass = marker[2]
                    #self.get_logger().info('Marker Object:  {}'.format(marker_objectclass))
                    if marker_objectclass == visible_objectclass:
                        if not ret[i] < np.inf:
                            ret[i] = visible_distance
        
        """
        
        for object in visible_objects:
            matched_idx = self.data_association(object)
            if matched_idx != -1:
                self.get_logger().info('Found match: {}'.format(matched_idx))
                object_visible_distance = np.linalg.norm(object.position3d[0:2]) # erstmal nur x und y
                ret[matched_idx] = object_visible_distance
        
        #self.get_logger().info('Calculated Distance:  {}'.format(ret))
        return ret
    
    def data_association(self, obj):
        """
        ## Taken from the database_manager.py ## 

        The detection 'obj' is compared to each object in self.object_database. 
        If both have the same class and form and are close to each other in space 
        (or, preferrably, they have the same trackingid and are close in space), then they
        are associated with each other.

        If these conditions are not fulfilled for any object in self.object_database, -1 is returned.

        :param obj: the detected object.

        :returns: the index of the object in object_database associated with obj, or -1 if there is no such object.
        """
        
        
        
        min_da_iou = 0.01

        if len(self.landmarks) == 0:
            return -1
        else:
            # Use the id of the object tracked over time to associate it with
            # an object in the object_database.
            # Use the object class (not tracked over time) and
            # the object's overlap with the ones
            # in the object_database to associate the two.
            association_candidates = []

            for num, item in enumerate(self.landmarks):
                #self.get_logger().info('{}'.format(self.last_position))
                # Its not possible to use the direct odometry position here.
                # If you do so, the camera creates its frame at the 0, 0, 0 position and the correct 
                # localisation from the pf is shifted to 0, 0, 0
                # Using the xs_history instead can cause bugs especially in the kidnapped robot problem
                # This has to be fixed in the future!
                diff_x = float(item[0]) - float(obj.position3d[0])
                diff_y = float(item[1]) - float(obj.position3d[1])
                diff_z = float(item[2]) - float(obj.position3d[2])
                dist = np.sqrt(diff_x**2 + diff_y**2 + diff_z**2)
                self.get_logger().info('{}-{} ## [DA] Calc distance: {}'.format(obj.objectclass, item[3], dist))
                #self.get_logger().info('{}'.format(item))
                if (obj.objectclass == item[3]) and (dist < self.fusion_radius):
                        return num
                    
                elif obj.objectclass == item[3] and dist < 3*self.fusion_radius:
                    #self.get_logger().info('Calculating iou')

                    try:
                        intersection_vol, iou = box3d_overlap(
                            self.convert_bbox_points_to_array(obj.bb3d), 
                            self.convert_bbox_points_to_array(item[5])
                        )
                    except ValueError as error:
                        #print('Caught an error during iou calculation: {}'.format(error))
                        iou = 0.0

                    #self.get_logger().info('IOU: {}'.format(str(float(iou))))

                    if float(iou) > min_da_iou:
                        association_candidates.append([num, iou])

            # Associate the object with the one from the object_database that has the highest overlap, 
            # or with no object if it overlaps with no object from the object_database.
            iou_max = 0
            best_candidate = -1
                
            if len(association_candidates) != 0:
                for item in association_candidates:
                    if item[1] > iou_max:
                        iou_max = item[1]
                        best_candidate = item[0]
                self.get_logger().info('{} with iou {}'.format(best_candidate, iou_max))
                return best_candidate # idx
            else:
                return -1
    
    def convert_bbox_points_to_array(self, box):
        #self.get_logger().info('box: {}'.format(box))
        ret = []
        for point in box:
            ret.append([point.x, point.y, point.z])

        ret = np.array(ret).astype(np.float32)
        ret = torch.from_numpy(np.resize(ret, (1, 8, 3)))
        #self.get_logger().info('output: {}, shape: {}'.format(ret, ret.shape))
        return ret

def main(args=None):
    rclpy.init(args=args)
    node = ParticleFilterEstimator()
    rclpy.spin(node)
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()

if __name__=="__main__":
    main()