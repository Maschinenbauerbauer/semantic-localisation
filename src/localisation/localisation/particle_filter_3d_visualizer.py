# Tutorial from: https://github.com/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/12-Particle-Filters.ipynb
import rclpy
from rclpy.node import Node
from rclpy.time import Duration
from msgs.msg import ParticlesArrayMsg
from visualization_msgs.msg import Marker, MarkerArray

class ParticleFilter3dVisualizer(Node):

    def __init__(self):
        super().__init__('particle_filter_visualizer')

        self.subscription = self.create_subscription(
            ParticlesArrayMsg,
            "particlefilter/particles",
            self.visualize,
            1
        )

        self.publisher = self.create_publisher(
            MarkerArray,
            "particlefilter/rviz_markers",
            1
        )

        # Necessary to delete all markers on new startup!
        marker_array = MarkerArray()
        marker = Marker()
        marker.id = 0
        
        marker.action = marker.DELETEALL
        marker_array.markers.append(marker)
        self.publisher.publish(marker_array)
        self.get_logger().info("Visualizer running..")


    def visualize(self, msg):
        
        marker_array = MarkerArray()
        
        counter = 0
        for item in msg.particles:
            marker = self.generate_marker(msg.header, item, counter)
            marker_array.markers.append(marker)
            counter += 1

        self.get_logger().info("Publishing {} particles..".format(counter))
        self.publisher.publish(marker_array)
        pass

    def generate_marker(self, header, item, num):
        msg = Marker()
        msg.header = header
        msg.type = msg.SPHERE
        msg.action = msg.ADD
        msg.id = num
        msg.lifetime = Duration(seconds=1).to_msg()

        msg.scale.x = .03
        msg.scale.y = .03
        msg.scale.z = .03

        msg.color.a = 1.0
        msg.color.r = 1.0
        msg.color.g = 0.0
        msg.color.b = 0.0

        msg.pose.position.x, msg.pose.position.y, msg.pose.position.z  = float(item.position[0]), float(item.position[1]), float(item.position[2])
        msg.pose.orientation.x = msg.pose.orientation.y = msg.pose.orientation.z = 0.0
        msg.pose.orientation.w = 1.0

        return msg


def main(args=None):
    rclpy.init(args=args)
    node = ParticleFilter3dVisualizer()
    rclpy.spin(node)
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()

if __name__=="__main__":
    main()