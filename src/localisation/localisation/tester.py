import rclpy
from rclpy.node import Node

import numpy as np

from msgs.msg import DatabaseMsg, ObjectArray, DatabaseObject, ObjectInformation, DummyInertiaMsg
from geometry_msgs.msg import Point
from std_msgs.msg import Header

def landmarks_get_case(case_nr):
    dummy_msg = None
    if case_nr == 1:
        dummy_msg = landmarks_case_1()
    elif case_nr == 2:
        dummy_msg = landmarks_case_2()
    elif case_nr == 3:
        dummy_msg = landmarks_case_3()
    elif case_nr == 4:
        dummy_msg = landmarks_case_4()
    return dummy_msg

def landmarks_case_1():
    "Simulates three objects directly in front on a line"
    dummy_msg = DatabaseMsg()
    dummy_msg.header = Header(); dummy_msg.header.frame_id = 'map'

    # Add dummy landmarks
    obj2add = DatabaseObject()
    obj2add.objectclass = 'cat'
    obj2add.trackingid = 0
    obj2add.position = np.array([1.0, 0.0, 0.0], dtype=np.float32)
    p = Point()
    p.x = 1.0; p.y = 0.0; p.z = 0.0;
    obj2add.bb3d = list(np.array([p]* 8))
    dummy_msg.objects.append(obj2add)

    obj2add = DatabaseObject()
    obj2add.objectclass = 'dog'
    obj2add.trackingid = 1
    obj2add.position = np.array([1.0, 1.0, 0.0], dtype=np.float32)
    p = Point()
    p.x = 1.0; p.y = 1.0; p.z = 0.0
    obj2add.bb3d = list(np.array([p]* 8))
    dummy_msg.objects.append(obj2add)

    obj2add = DatabaseObject()
    obj2add.objectclass = 'horse'
    obj2add.trackingid = 2
    obj2add.position = np.array([0.5, -1.0, 0.0], dtype=np.float32)
    p = Point()
    p.x = 1.0; p.y = -1.0; p.z = 0.0
    obj2add.bb3d = list(np.array([p]* 8))
    dummy_msg.objects.append(obj2add)
    return dummy_msg

def landmarks_case_2():
    "Simulates four objects around"
    dummy_msg = DatabaseMsg()
    dummy_msg.header = Header(); dummy_msg.header.frame_id = 'map'

    # Add dummy landmarks
    obj2add = DatabaseObject()
    obj2add.objectclass = 'cat'
    obj2add.trackingid = 0
    obj2add.position = np.array([1.0, 1.0, 0.0], dtype=np.float32)
    p = Point()
    p.x = 1.0; p.y = 1.0; p.z = 0.0;
    obj2add.bb3d = list(np.array([p]* 8))
    dummy_msg.objects.append(obj2add)

    obj2add = DatabaseObject()
    obj2add.objectclass = 'dog'
    obj2add.trackingid = 1
    obj2add.position = np.array([1.0, -1.0, 0.0], dtype=np.float32)
    p = Point()
    p.x = 1.0; p.y = -1.0; p.z = 0.0;
    obj2add.bb3d = list(np.array([p]* 8))
    dummy_msg.objects.append(obj2add)

    obj2add = DatabaseObject()
    obj2add.objectclass = 'horse'
    obj2add.trackingid = 2
    obj2add.position = np.array([-1.0, 1.0, 0.0], dtype=np.float32)
    p = Point()
    p.x = -1.0; p.y = 1.0; p.z = 0.0;
    obj2add.bb3d = list(np.array([p]* 8))
    dummy_msg.objects.append(obj2add)

    obj2add = DatabaseObject()
    obj2add.objectclass = 'sheep'
    obj2add.trackingid = 3
    obj2add.position = np.array([-1.0, -1.0, 0.0], dtype=np.float32)
    p = Point()
    p.x = -1.0; p.y = -1.0; p.z = 0.0;
    obj2add.bb3d = list(np.array([p]* 8))
    dummy_msg.objects.append(obj2add)

    return dummy_msg

def landmarks_case_3():
    "Simulates one object"
    dummy_msg = DatabaseMsg()
    dummy_msg.header = Header(); dummy_msg.header.frame_id = 'map'

    # Add dummy landmarks
    obj2add = DatabaseObject()
    obj2add.objectclass = 'cat'
    obj2add.trackingid = 0
    obj2add.position = np.array([1.0, 0.0, 0.0], dtype=np.float32)
    p = Point()
    p.x = 1.0; p.y = 0.0; p.z = 0.0;
    obj2add.bb3d = list(np.array([p]* 8))
    dummy_msg.objects.append(obj2add)

    return dummy_msg

count = 0
def landmarks_case_4():
    global count
    "Simulates four objects around"
    dummy_msg = DatabaseMsg()
    dummy_msg.header = Header(); dummy_msg.header.frame_id = 'map'

    stepsize = .01 # This is coupled to velocity values!! This means every iteration, move .01 meters in x direction!

    # Add dummy landmarks
    obj2add = DatabaseObject()
    obj2add.objectclass = 'cat'
    obj2add.trackingid = 0
    obj2add.position = np.array([1.0+count*stepsize, 1.0, 0.0], dtype=np.float32)
    p = Point()
    p.x = 1.0; p.y = 1.0; p.z = 0.0;
    obj2add.bb3d = list(np.array([p]* 8))
    dummy_msg.objects.append(obj2add)

    obj2add = DatabaseObject()
    obj2add.objectclass = 'dog'
    obj2add.trackingid = 1
    obj2add.position = np.array([1.0+count*stepsize, -1.0, 0.0], dtype=np.float32)
    p = Point()
    p.x = 1.0; p.y = -1.0; p.z = 0.0;
    obj2add.bb3d = list(np.array([p]* 8))
    dummy_msg.objects.append(obj2add)

    obj2add = DatabaseObject()
    obj2add.objectclass = 'horse'
    obj2add.trackingid = 2
    obj2add.position = np.array([-1.0+count*stepsize, 1.0, 0.0], dtype=np.float32)
    p = Point()
    p.x = -1.0; p.y = 1.0; p.z = 0.0;
    obj2add.bb3d = list(np.array([p]* 8))
    dummy_msg.objects.append(obj2add)

    obj2add = DatabaseObject()
    obj2add.objectclass = 'sheep'
    obj2add.trackingid = 3
    obj2add.position = np.array([-1.0+count*stepsize, -1.0, 0.0], dtype=np.float32)
    p = Point()
    p.x = -1.0; p.y = -1.0; p.z = 0.0;
    obj2add.bb3d = list(np.array([p]* 8))
    dummy_msg.objects.append(obj2add)
    count += 1
    return dummy_msg


def measurements_get_case(case_nr):
    dummy_msg = None
    if case_nr == 1:
        dummy_msg = measurements_case_1()
    elif case_nr == 2:
        dummy_msg = measurements_case_2()
    elif case_nr == 3:
        dummy_msg = measurements_case_3()
    elif case_nr == 4:
        dummy_msg = measurements_case_4()
    return dummy_msg

def measurements_case_1():
    "Simulates three objects directly in front on a line"
    dummy_msg = ObjectArray()

    noise2add = np.array([0.0, 0.0, 0.0], dtype=np.float32)
    #noise2add = np.random.normal(0, .2 ,3)
    # 0 is the mean of the normal distribution you are choosing from
    # .2 is the standard deviation of the normal distribution
    # 3 is the number of elements you get in array noise

    # Add dummy landmarks
    obj2add = ObjectInformation()
    obj2add.objectclass = 'cat'
    obj2add.trackingid = 0
    obj2add.position3d = np.add(np.array([1.0, 0.0, 0.0], dtype=np.float32), noise2add)
    dummy_msg.objects.append(obj2add)

    obj2add = ObjectInformation()
    obj2add.objectclass = 'dog'
    obj2add.trackingid = 1
    obj2add.position3d = np.add(np.array([1.0, 1.0, 0.0], dtype=np.float32), noise2add)
    dummy_msg.objects.append(obj2add)

    obj2add = ObjectInformation()
    obj2add.objectclass = 'horse'
    obj2add.trackingid = 2
    obj2add.position3d = np.add(np.array([0.5, -1.0, 0.0], dtype=np.float32), noise2add)
    dummy_msg.objects.append(obj2add)

    inertia_msg = DummyInertiaMsg()
    inertia_msg.linear_velocity_x = inertia_msg.linear_velocity_y = inertia_msg.linear_velocity_z = 0.0
    inertia_msg.angular_velocity = 0.0
    return dummy_msg, inertia_msg


def measurements_case_2():
    "Simulates four objects around"
    dummy_msg = ObjectArray()

    noise2add = np.array([0.0, 0.0, 0.0], dtype=np.float32)
    #noise2add = np.random.normal(0, .2 ,3)
    # 0 is the mean of the normal distribution you are choosing from
    # .2 is the standard deviation of the normal distribution
    # 3 is the number of elements you get in array noise

    # Add dummy landmarks
    obj2add = ObjectInformation()
    obj2add.objectclass = 'cat'
    obj2add.trackingid = 0
    obj2add.position3d = np.add(np.array([1.0, 1.0, 0.0], dtype=np.float32), noise2add)
    dummy_msg.objects.append(obj2add)

    obj2add = ObjectInformation()
    obj2add.objectclass = 'dog'
    obj2add.trackingid = 1
    obj2add.position3d = np.add(np.array([1.0, -1.0, 0.0], dtype=np.float32), noise2add)
    dummy_msg.objects.append(obj2add)

    obj2add = ObjectInformation()
    obj2add.objectclass = 'horse'
    obj2add.trackingid = 2
    obj2add.position3d = np.add(np.array([-1.0, 1.0, 0.0], dtype=np.float32), noise2add)
    dummy_msg.objects.append(obj2add)

    obj2add = ObjectInformation()
    obj2add.objectclass = 'sheep'
    obj2add.trackingid = 3
    obj2add.position3d = np.add(np.array([-1.0, -1.0, 0.0], dtype=np.float32), noise2add)
    dummy_msg.objects.append(obj2add)

    inertia_msg = DummyInertiaMsg()
    inertia_msg.linear_velocity_x = inertia_msg.linear_velocity_y = inertia_msg.linear_velocity_z = 0.0
    inertia_msg.angular_velocity = 0.0
    return dummy_msg, inertia_msg


def measurements_case_3():
    "Simulates one object"
    dummy_msg = ObjectArray()

    noise2add = np.array([0.0, 0.0, 0.0], dtype=np.float32)
    #noise2add = np.random.normal(0, .2 ,3)
    # 0 is the mean of the normal distribution you are choosing from
    # .2 is the standard deviation of the normal distribution
    # 3 is the number of elements you get in array noise

    # Add dummy landmarks
    obj2add = ObjectInformation()
    obj2add.objectclass = 'cat'
    obj2add.trackingid = 0
    obj2add.position3d = np.add(np.array([1.0, 0.0, 0.0], dtype=np.float32), noise2add)
    dummy_msg.objects.append(obj2add)

    inertia_msg = DummyInertiaMsg()
    inertia_msg.linear_velocity_x = inertia_msg.linear_velocity_y = inertia_msg.linear_velocity_z = 0.0
    inertia_msg.angular_velocity = 0.0
    return dummy_msg, inertia_msg


def measurements_case_4():
    "Simulates four objects around"
    dummy_msg = ObjectArray()

    noise2add = np.array([0.0, 0.0, 0.0], dtype=np.float32)
    #noise2add = np.random.normal(0, .2 ,3)
    # 0 is the mean of the normal distribution you are choosing from
    # .2 is the standard deviation of the normal distribution
    # 3 is the number of elements you get in array noise

    # Add dummy landmarks
    obj2add = ObjectInformation()
    obj2add.objectclass = 'cat'
    obj2add.trackingid = 0
    obj2add.position3d = np.add(np.array([1.0, 1.0, 0.0], dtype=np.float32), noise2add)
    dummy_msg.objects.append(obj2add)

    obj2add = ObjectInformation()
    obj2add.objectclass = 'dog'
    obj2add.trackingid = 1
    obj2add.position3d = np.add(np.array([1.0, -1.0, 0.0], dtype=np.float32), noise2add)
    dummy_msg.objects.append(obj2add)

    obj2add = ObjectInformation()
    obj2add.objectclass = 'horse'
    obj2add.trackingid = 2
    obj2add.position3d = np.add(np.array([-1.0, 1.0, 0.0], dtype=np.float32), noise2add)
    dummy_msg.objects.append(obj2add)

    obj2add = ObjectInformation()
    obj2add.objectclass = 'sheep'
    obj2add.trackingid = 3
    obj2add.position3d = np.add(np.array([-1.0, -1.0, 0.0], dtype=np.float32), noise2add)
    dummy_msg.objects.append(obj2add)

    inertia_msg = DummyInertiaMsg()
    # Is calculated from stepsize. every .05 seconds move .01m
    inertia_msg.linear_velocity_x = .01/.05 # this can be any value
    inertia_msg.linear_velocity_y = inertia_msg.linear_velocity_z = 0.0
    inertia_msg.angular_velocity = 0.0
    return dummy_msg, inertia_msg


"""
Sends dummy values for specific topics to test the localisation methods.
"""
class Tester(Node):

    def __init__(self):
        super().__init__('Tester')
        """
        1: three objects in a line
        2: four objects around, static
        3: One object
        4: Moving with four objects around (and all can be seen! -> impossible in reality)
        """
        self.case = 4

        self.count = 0
        timer_period = .1  # seconds, this is atm coupled to the time in the particle filter!!
        self.timer = self.create_timer(timer_period, self.create_dummy_landmarks)
        self.timer = self.create_timer(timer_period, self.create_dummy_measurements)

        self.landmarks_publisher = self.create_publisher(DatabaseMsg, 'mapping/current_db', 1)
        self.measurements_publisher = self.create_publisher(ObjectArray, 'segmentation/segmented_detections', 1)
        self.inertial_meas_publisher = self.create_publisher(DummyInertiaMsg, 'inertia', 1)
        self.get_logger().info("Remember to start the static transform publisher: ros2 run tf2_ros static_transform_publisher 0 0 0 0 0 0 map world")

    def create_dummy_landmarks(self):
        dummy_msg = landmarks_get_case(self.case)
        self.landmarks_publisher.publish(dummy_msg)
        pass
    
    def create_dummy_measurements(self):
        dummy_msg, velocity_msg = measurements_get_case(self.case)
        self.measurements_publisher.publish(dummy_msg)
        self.inertial_meas_publisher.publish(velocity_msg)
        pass


def main(args=None):
    rclpy.init(args=args)

    node = Tester()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
