from turtle import distance
import zipimport

from matplotlib.pyplot import sci
import rclpy
from rclpy.node import Node
from rclpy.duration import Duration

from std_msgs.msg import String
from bboxes_ex_msgs.msg import BoundingBoxes
from visualization_msgs.msg import MarkerArray, Marker
from msgs.msg import ObjectArray, ObjectInformation, DatabaseMsg
from geometry_msgs.msg import PointStamped, TransformStamped
import tf2_geometry_msgs
import tf_transformations
import time

import numpy as np

from sensor_msgs.msg import CameraInfo
import image_geometry
import tf2_ros
from tf2_ros.buffer import Buffer
from tf2_ros import TransformException
from tf2_ros.transform_listener import TransformListener
from tf2_ros.transform_broadcaster import TransformBroadcaster

from localisation.virtual_frame import VirtualFramePublisher

from geneticalgorithm import geneticalgorithm as ga
import scipy.optimize as optimize

import cv2

class PoseEstimator(Node):

    def __init__(self):
        super().__init__('pose_estimator')


        # Attributes
        self.current_database = None
        self.last_yolo_msg = None
        self.last_markers_msg = None#
        self.camera_info = None # TODO: This is not perfect, but as for now, there is no wait for message in ROS2
        self.x_initial_guess = [0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]
        self.best_score = np.inf

        # TF2
        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)
        self.tf_broadcaster = TransformBroadcaster(self)

        # Get the camera information for reprojection purposes
        self.camera_info_sub = self.create_subscription(
            CameraInfo,
            "/zed2/zed_node/rgb/camera_info",
            self.get_camera_info,
            10
        )    

        # As the minimization terminates at a specific delta epsilon, the score of the 
        # objective function can be influenced via this parameter
        self.DISTANCE_SCALING = 1.0
        self.declare_parameter('distance-scaling', self.DISTANCE_SCALING)
        rclpy.parameter.Parameter(
            'distance-scaling',
            rclpy.Parameter.Type.DOUBLE,
            self.DISTANCE_SCALING
        )

        # Subscription and Publisher
        self.markers_subscription = self.create_subscription(
            MarkerArray,
            'yolov5/bounding_boxes',
            self.markers_callback,
            10
        )
        self.yolo_subscription = self.create_subscription(
            ObjectArray,
            'yolov5/detections_yolo',
            self.yolo_callback,
            10
        )
        self.database_subscription = self.create_subscription(
            DatabaseMsg,
            'mapping/current_db',
            self.database_update,
            10
        )

        self.frame_pub = self.create_publisher(
            TransformStamped,
            'localisation/transform_params',
            1
        )
        
        q1 = np.array(tf_transformations.quaternion_from_euler(0, np.deg2rad(90.0), 0))
        q2 = np.array(tf_transformations.quaternion_from_euler(0, 0, -np.deg2rad(90.0)))
        q = tf_transformations.quaternion_multiply(q1, q2)
        self.create_new_transform(0.0, 0.0, 0.0, q[0], q[1], q[2], q[3]) # Initial setting, so the frame is really published for lookup transform
        self.database_subscription  # prevent unused variable warning
        self.yolo_subscription  # prevent unused variable warning
        self.markers_subscription  # prevent unused variable warning

    def get_camera_info(self, msg):
        self.camera_info = msg
    

    def database_update(self, msg):
        "Updates the database from time to time. Does not need to be super fast."
        self.current_database = msg

        self.DISTANCE_SCALING = float(self.get_parameter('distance-scaling').get_parameter_value().double_value)
        
        #print(object_in_db)

        if self.camera_info is not None:
            #self.get_logger().info('Has camera info')
            if self.last_yolo_msg is not None:
                    #self.get_logger().info('Has yolo')   

                def objective_function(x0):      
                    # Update the virtual frame

                    #self.get_logger().info(str(x0))

                    updated_transformation = self.create_new_transform(
                        x0[0],
                        x0[1],
                        x0[2],
                        x0[3],
                        x0[4],
                        x0[5],
                        x0[6]
                    )
                    new_buffer = Buffer()
                    #self.get_logger().info('new_transf_trans: ' + str(updated_transformation.transform.translation))
                    new_buffer.set_transform(updated_transformation, 'None')
                
                    virtual_camera = image_geometry.PinholeCameraModel()
                    virtual_camera.fromCameraInfo(self.camera_info)
                    virtual_camera.tf_frame = 'virtual_frame'
                    #self.get_logger().info(str(virtual_camera.tfFrame()))

                    # Associate each seen object by yolo with object from db.
                    # Do this by specifying a given radius in map frame to search for the items.

                    """
                    YOLO SPECIFIC STUFF
                    """

                    #self.get_logger().info('Yolo msg:' + str(self.last_yolo_msg))

                    unique_fov_objects = self.get_unique_fov_objects(self.last_yolo_msg)
                    unique_fov_objects_w_center = []

                    for obj in unique_fov_objects:
                        center_x = (obj.bb[0] + obj.bb[2])/2
                        center_y = (obj.bb[1] + obj.bb[3])/2
                        unique_fov_objects_w_center.append({
                            'class': obj.objectclass,
                            'pixel_center': (center_x, center_y)
                            })
                    
                    self.last_fov_objects = unique_fov_objects_w_center # debugging
                    #self.get_logger().info('FOV obj:' + str(unique_fov_objects_w_center))
                    # bb = [min_x, min_y, max_x, max_y]

                    """
                    DB SPECIFIC STUFF
                    """

                    max_x = x0[0] + 4 # meter
                    min_x = x0[0]
                    max_y = x0[1] + 2
                    min_y = x0[1] - 2
                    max_z = x0[2] + 2
                    min_z = x0[2] - 2

                    # Get database objects of objects in fov

                    objects_in_db = [{
                        'class': i.objectclass,
                        'centroid': i.position.astype(float)
                    } for i in self.current_database.objects]

                    filtered_object_from_database = [x for x in objects_in_db if self.check_centroid(x['centroid'], [min_x, max_x, min_y, max_y, min_z, max_z])]
                    #self.get_logger().info(str(filtered_object_from_database))

                    # For every object type, check if unique
                    object_count = self.get_db_object_count_hierarchy(filtered_object_from_database)

                    unique_db_items_w_centroid = []
                    for x in filtered_object_from_database:
                        if object_count[x['class']] == 1:
                            # Unique item can be matched
                            unique_db_items_w_centroid.append(x)

                    #self.get_logger().info('Unique DB obj:' + str(unique_db_items_w_centroid))

                    
                    """Matching the objects"""
                    


                    # List with respective pixel points for each object
                    virtual_pixel_points = []

                    for obj in unique_db_items_w_centroid:
                        centroid_point = PointStamped() # In map frame
                        centroid_point.header.stamp = updated_transformation.header.stamp
                        centroid_point.header.frame_id = 'map'
                        [centroid_point.point.x, centroid_point.point.y, centroid_point.point.z] = obj['centroid']
                        
                        virtual_frame_point = new_buffer.transform(centroid_point, 'virtual_frame')

                        # Coordinates like https://hedivision.github.io/Pinhole.html
                        #self.get_logger().info('Centroid: ' + str([centroid_point.point.y, centroid_point.point.z, centroid_point.point.x]))
                        
                        # Project the 3D information from map onto the pixels of the camera.
                        # Holds the pixel where the centroid would be displayed on an image.

                        # The virtual camera needs a different coordinate system:
                        #x = -virtual_frame_point.point.y
                        #y = -virtual_frame_point.point.z
                        #z = +virtual_frame_point.point.x
                        x = -virtual_frame_point.point.y
                        y = -virtual_frame_point.point.z
                        z = virtual_frame_point.point.x

                        virtual_pixel_point = virtual_camera.project3dToPixel((x, y, z))
                        obj['virtual_pixel_point'] = virtual_pixel_point
                        #self.get_logger().info('VPP: ' + str(virtual_pixel_point))
                        

                    # Calculate distance from point to every other point
                    #self.get_logger().info(str(unique_db_items_w_centroid))

                    # Falls keine unique items
                    if not (unique_db_items_w_centroid and unique_fov_objects_w_center):
                        score = np.inf
                        return score

                    score = 0.0
                    count = 0
                    for virtual_frame_object in unique_db_items_w_centroid:
                        for fov_center_objects in unique_fov_objects_w_center:
                            if virtual_frame_object['class'] == fov_center_objects['class']:
                                # Match virtual item with corresponding fov item
                                #self.get_logger().info('Substr: '+str(virtual_frame_object['virtual_pixel_point']) + ' ' + str(fov_center_objects['pixel_center']))
                                distance = self.calculate_euclid_distance(virtual_frame_object['virtual_pixel_point'], fov_center_objects['pixel_center'])
                                score += distance * self.DISTANCE_SCALING
                                count += 1

                    if count < 2:
                        score = np.inf
                        return score

                    #self.get_logger().info('Score: ' + str(score))

                    "Debugging with cv2"
                    img = np.ones((720, 1280, 3))
                    for i in unique_fov_objects_w_center:
                        center = (int(i['pixel_center'][0]), int(i['pixel_center'][1]))
                        cv2.putText(img, i['class'], (int(i['pixel_center'][0])+10, int(i['pixel_center'][1])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1, 8)
                        cv2.circle(img, center, 10, (0,255,0), thickness=1, lineType=8, shift=0)
                    for i in unique_db_items_w_centroid:
                        center = (int(i['virtual_pixel_point'][0]), int(i['virtual_pixel_point'][1]))
                        cv2.putText(img, i['class'], (int(i['virtual_pixel_point'][0])+10, int(i['virtual_pixel_point'][1])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1, 8)
                        cv2.circle(img, center, 10, (0,0,255), thickness=1, lineType=8, shift=0)
                    
                    cv2.imshow('img', img)
                    cv2.waitKey(1)

                    return score/count

                # Set this to last known odometry position later
                q1 = np.array(tf_transformations.quaternion_from_euler(0, np.deg2rad(90.0), 0))
                q2 = np.array(tf_transformations.quaternion_from_euler(0, 0, -np.deg2rad(90.0)))
                q = tf_transformations.quaternion_multiply(q1, q2)

                boundary_trans = 0.6
                boundary_angle = 0.2
                param_grid = np.array([
                    (self.x_initial_guess[0]-boundary_trans, self.x_initial_guess[0]+boundary_trans),
                    (self.x_initial_guess[1]-boundary_trans, self.x_initial_guess[1]+boundary_trans),
                    (self.x_initial_guess[2]-boundary_trans, self.x_initial_guess[2]+boundary_trans),
                    (self.x_initial_guess[3]-boundary_angle, self.x_initial_guess[3]+boundary_angle),
                    (self.x_initial_guess[4]-boundary_angle, self.x_initial_guess[4]+boundary_angle),
                    (self.x_initial_guess[5]-boundary_angle, self.x_initial_guess[5]+boundary_angle),
                    (self.x_initial_guess[6]-boundary_angle, self.x_initial_guess[6]+boundary_angle)
                ])
                """
                algorithm_param = {
                    'max_num_iteration': 20,
                    'population_size':20,
                    'mutation_probability':0.6,
                    'elit_ratio': 0.01,
                    'crossover_probability': 0.5,
                    'parents_portion': 0.3,
                    'crossover_type':'uniform',
                    'max_iteration_without_improv':None}
                
                model=ga(
                    function=objective_function,
                    dimension=3,
                    variable_type='real',
                    variable_boundaries=param_grid,
                    algorithm_parameters=algorithm_param
                    )

                model.run()
                
                result = optimize.differential_evolution(
                    func=objective_function, 
                    bounds=param_grid, 
                    strategy='best1bin', 
                    maxiter=100, popsize=8)pip install optimparallel
                """


                # Does not work because it tries to converge to fast, we need a global optimizer
                result = optimize.minimize(fun=objective_function, x0=self.x_initial_guess, method="Nelder-Mead", #bounds=param_grid, 
                    options={'apative': True}
                )

                """
                result = optimize.brute(
                    func=objective_function,
                    ranges=param_grid,
                    Ns=3
                )
                """
                #self.get_logger().info('res:' + str(result))
                
                #result = optimize.minimize(objective_function, x0=x_initial_guess, method="Nelder-Mead", tol=1e-8, bounds=[
                #    (-boundary, boundary), (-boundary, boundary), (-boundary, boundary)], options={'adaptive': True, 'disp': True}
                #    )
        pass


    def create_new_transform(self, t_x, t_y, t_z, r_x, r_y, r_z, r_w):
        t = TransformStamped()
        #t.header.stamp = self.get_clock().now().to_msg()
        t.header.frame_id = 'map'
        t.child_frame_id = 'virtual_frame'
        t.transform.translation.x = t_x
        t.transform.translation.y = t_y
        t.transform.translation.z = t_z
        t.transform.rotation.x = r_x
        t.transform.rotation.y = r_y
        t.transform.rotation.z = r_z
        t.transform.rotation.w = r_w
        self.frame_pub.publish(t)
        return t


    def check_centroid(self, centr, ranges):
        #self.get_logger().info(str(ranges))
        x_range = (int(ranges[0]), int(ranges[1]))
        y_range = (int(ranges[2]), int(ranges[3]))
        z_range = (int(ranges[4]), int(ranges[5]))

        #self.get_logger().info(str(x_range)+str(y_range)+str(z_range))
        
        flag_x = False
        flag_y = False
        flag_z = False

        if x_range[0] <= centr[0] <= x_range[1]:
            flag_x = True
        if y_range[0] <= centr[1] <= y_range[1]:
            flag_y = True
        if z_range[0] <= centr[2] <= z_range[1]:
            flag_z = True

        #self.get_logger().info(str(centr))
        #self.get_logger().info(str(flag_x)+str(flag_y)+str(flag_z))

        if flag_x and flag_y and flag_z:
            return True
        else:
            return False

    
    def calculate_euclid_distance(self, p0, p1):
        #self.get_logger().info('Points: ' + str(p0) + ' ' + str(p1))
        p0 = np.array(p0)
        p1 = np.array(p1)
        dist = np.linalg.norm(p0-p1)
        #self.get_logger().info('Dist: ' + str(dist))
        return dist


    def get_unique_fov_objects(self, object_array):
        ret = []
        counts = {}
        for obj in object_array.objects:
            if obj.objectclass in counts.keys():
                counts[obj.objectclass] += 1
            else:
                counts[obj.objectclass] = 1
            
        filtered_unique_classes = [key for key, value in counts.items() if value == 1]
        for obj in object_array.objects:
            if obj.objectclass in filtered_unique_classes:
                ret.append(obj)
        
        return ret


    def get_db_object_count_hierarchy(self, objects_in_db):
        count_dict = {}
        for x in objects_in_db:
            if x['class'] in count_dict.keys():
                count_dict[x['class']] += 1
            else:
                count_dict[x['class']] = 1

        # Last item has biggest count
        object_hierarchy = dict(reversed(sorted(count_dict.items(), key=lambda item: item[1])))
        return object_hierarchy

    def yolo_callback(self, msg):
        "Every frame, try to find pose in map."
        self.last_yolo_msg = msg
        pass

    def markers_callback(self, msg):
        self.last_markers_msg = msg
        pass


def main(args=None):
    rclpy.init(args=args)

    estimator = PoseEstimator()

    rclpy.spin(estimator)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    estimator.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
