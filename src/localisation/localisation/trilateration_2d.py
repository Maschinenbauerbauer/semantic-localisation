import rclpy
from rclpy.node import Node

import numpy as np

from msgs.msg import DatabaseMsg, ObjectArray

import cv2
cv2_debugging = True
scale=50

def draw_grid(img, grid_shape, color=(0, 0, 0), thickness=1):
    h, w, _ = img.shape
    rows, cols = grid_shape
    dy, dx = h / rows, w / cols

    # draw vertical lines
    for x in np.linspace(start=dx, stop=w-dx, num=cols-1):
        x = int(round(x))
        cv2.line(img, (x, 0), (x, h), color=color, thickness=thickness)

    # draw horizontal lines
    for y in np.linspace(start=dy, stop=h-dy, num=rows-1):
        y = int(round(y))
        cv2.line(img, (0, y), (w, y), color=color, thickness=thickness)

    return img

class Trilateration(Node):

    def __init__(self):
        super().__init__('trilateration')
        self.database_subscription = self.create_subscription(
            DatabaseMsg,
            'mapping/current_db',
            self.callback,
            1)

        self.segment_subscription = self.create_subscription(
            ObjectArray,
            'segmentation/segmented_detections',
            self.update_visible_objects,
            1)

        self.visible_objects = []

    def update_visible_objects(self, msg):
        visible_objects = []
        for item in msg.objects:
            visible_objects.append([item.objectclass, (int(640+item.position3d[0]*scale), int(360+item.position3d[1]*scale))])

        self.visible_objects = visible_objects
        #self.get_logger().info(str(self.visible_objects))
        pass

    def callback(self, database_map):
        if self.visible_objects:
            if cv2_debugging:
                img = np.ones((720, 1280, 3)) 
                draw_grid(img, grid_shape=(10, 10))
                cv2.arrowedLine(img, (640,360), (640+50, 360), (0, 0, 255), 1, tipLength = .2)
                cv2.putText(img, 'X', (640+50+10, 360), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1, 8)
                cv2.arrowedLine(img, (640, 360), (640, 360+50), (0, 255, 0), 1, tipLength = .2)
                cv2.putText(img, 'Y', (640, 360+50+10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1, 8)
            
            unique_object_class_names = []

            for item in database_map.objects:
                if list(map(lambda x: x.objectclass, database_map.objects)).count(item.objectclass) == 1:
                    unique_object_class_names.append(item)

            estimated_vehicle_positions = []
            for item in unique_object_class_names:
                # First, only take 2D coordinates x and y
                if cv2_debugging:
                    # 0,0 is 360, 720
                    center = (int(1280/2+item.position[0]*scale), int(720/2+item.position[1]*scale))
                    #self.get_logger().info(str(item))
                    cv2.putText(img, item.objectclass, (center[0]+10, center[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1, 8)
                    cv2.circle(img, center, 5, (0, 255, 0), thickness=1, lineType=8, shift=0)

                def get_item(objectclass, l):
                    for x in l:
                        name = x[0]
                        if name == objectclass:
                            return x

                world_vector = np.array([center])
                visible_item = get_item(item.objectclass, self.visible_objects)
                if visible_item:
                    visible_vector = np.array([visible_item[1][0], visible_item[1][1]])
                    #self.get_logger().info(str(visible_vector))
                    estimated_vehicle_positions.append(np.add( 
                            np.array([1280/2, 720/2]),
                            np.add(world_vector, (-1)*visible_vector).tolist()[0]
                        )
                    )

            x, y, count = 0, 0, 0
            for pos in estimated_vehicle_positions:
                #self.get_logger().info('Pos: {}'.format(pos))
                x += pos[0]
                y += pos[1]
                count += 1

            estimated_vehicle_position = (x/count, y/count) if count > 0 else None
            self.get_logger().info('Estimated vehicle position: {}'.format(estimated_vehicle_position))

            if cv2_debugging and count > 0:
                cv2.circle(img, (int(estimated_vehicle_position[0]), int(estimated_vehicle_position[1])), 5, (255,0,0), thickness=1, lineType=8, shift=0)

            if cv2_debugging and count > 0:
                cv2.imshow('img', img)
                cv2.waitKey(1)
            

def main(args=None):
    rclpy.init(args=args)

    node = Trilateration()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()