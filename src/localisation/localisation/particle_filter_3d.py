# Tutorial from: https://github.com/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/12-Particle-Filters.ipynb
from socket import MsgFlag
import rclpy
from rclpy.node import Node
from random import random
import numpy as np
from numpy.linalg import norm
from numpy.random import uniform, randn
from filterpy.monte_carlo import systematic_resample
import scipy
from scipy import stats
from msgs.msg import DatabaseMsg, ObjectArray, ParticleMsg, ParticlesArrayMsg, DummyInertiaMsg
from std_msgs.msg import Header, String, Float32
from nav_msgs.msg import Odometry
import torch
from pytorch3d.ops import box3d_overlap
from scipy.spatial.transform import Rotation


import cv2
cv2_debugging = True

def create_uniform_particles(x_range, y_range, z_range, hdg_range, N):
    # We maintain an x, y and heading of the robot. Heading only accounts for an angle. N=Number of particles.
    particles = np.empty((N, 4))
    particles[:, 0] = uniform(x_range[0], x_range[1], size=N)
    particles[:, 1] = uniform(y_range[0], y_range[1], size=N)
    particles[:, 2] = uniform(z_range[0], z_range[1], size=N)
    particles[:, 3] = uniform(hdg_range[0], hdg_range[1], size=N)
    return particles


def create_gaussian_particles(mean, std, N):
    # This can be used, if the initial position of the robot is known.
    particles = np.empty((N, 4))
    particles[:, 0] = mean[0] + (randn(N) * std[0])
    particles[:, 1] = mean[1] + (randn(N) * std[1])
    particles[:, 2] = mean[2] + (randn(N) * std[2])
    particles[:, 3] = mean[3] + (randn(N) * std[3])
    return particles


def predict(particles, u, std, dt=.1):
    """ move according to control input u (heading change, velocity)
    with noise Q (std heading change, std velocity)`"""

    """ Dies berechnet den Belief des Systems! 
    Wir berechnen das hier so, dass wir die vorherige Position um die Headingpositionsänderung (v*dt) 
    verschieben und zusätzlich noch einen Noise addieren.
    """

    # we need to add noise, because the movement is not 100% accurate!

    N = len(particles)
    # update heading
    particles[:, 3] += u[3] + (randn(N) * std[1])

    # move in the (noisy) commanded direction
    for i in range(0, particles.shape[1]-1):
        """randn(N) zieht N Samples aus einer Normalverteilung mit Std=1.0, also x-Werte!
        Multiplizieren wir diese Werte mit einem Faktor, können wir die neue Position - 
        errechnet aus v*dt - um diesen idealen Wert normalverteilt streuen.
        """
        tuning_factor = 0.5
        dist = (u[i] * dt) + (randn(N) * std[1]) # distance of the noisy movement vector
        particles[:, i] += dist*tuning_factor


def update(particles, weights, measured_distances, R1, landmarks, measured_angles, R2):
    # R: std_error
    """ Die Gewichtungen geben an, wie wahrscheinlich es ist, dass ein Partikel sich an der richtigen
    Roboterposition befindet. Nehmen wir also nun diese Gewichtungen und normalisieren sie, geben diese
    direkt die Wahrscheinlichkeitsverteilung (an diskreten Punkten) an.
    Genau das passiert in dieser Funktion: Mit np.lingalg.norm berechnen wir die Distanzen der Landmarks.
    Die Distanzen stellen in unserem Fall die indirekte Likelyhood-Funktion dar. Indirekt, weil es sich hierbei nicht um 
    Wahrscheinlichkeiten handelt. Diese Wahrscheinlichkeiten sind dann in den Gewichtungen vorhanden, weshalb diese die
    direkte Likelyhood-Funktion darstellen. Mulitpliziert mit dem prior (also des Zustands nach der Prediction) können wir 
    damit das Posterior nach der Bayes Regel errechnen.
    """
    # z = measurement distances
    # The update step calculates (=updates) the weights of the current particles
    landmarks = landmarks[:, 0:3].astype(np.float32) # array of x, y and z coordinates of the landmarks
    for i, landmark in enumerate(landmarks):
        
        # Step 1: Calculate the distance of each particle to the landmarks
        distance_error_x = np.abs(particles[:, 0] - landmark[0]) # Abweichung der Distanz
        distance_error_y = np.abs(particles[:, 1] - landmark[1]) # Abweichung der Distanz
        distance_error_z = np.abs(particles[:, 2] - landmark[2]) # Abweichung der Distanz

        # Remember! From our seen distances we can only calculate angles between -pi to +pi in reference to the measuring COSY
        # The particles instead give a heading from 0 ti +2*pi in reference to the World COSY!
        angle_error =  [np.abs(p[3] - np.arctan2(landmark[1], landmark[0])) for p in particles] # Abweichung des Winkels

        # Step 2: Set the weight according to the overall mean distance to the landmarks times a noise factor R
        """ scipy.stats.norm gibt uns einen stetige Repräsentation der Wahrscheinlichkeitsverteilung.
        Über R geben wir an, wie Wahrscheinlich es ist, dass unser Sensor die falschen Ergebnisse
        (Overshoot und Undershoot) liefert.
        Mit .pdf erzeugen wir aus der norm-Repräsentation eine Wahrscheinlichkeitsverteilungsfunktion
        aus welcher wir dann unser Posterior ziehen.
        """
        calc = scipy.stats.norm(distance_error_x, R1).pdf(np.abs(measured_distances[i][0]))
        if np.any(calc):
            weights *= calc
            weights /= sum(weights)
            
        calc = scipy.stats.norm(distance_error_y, R1).pdf(np.abs(measured_distances[i][1]))
        if np.any(calc):
            weights *= calc
            weights /= sum(weights)
            
        calc = scipy.stats.norm(distance_error_z, R1).pdf(np.abs(measured_distances[i][2]))
        if np.any(calc):
            weights *= calc
            weights /= sum(weights)
        
        #calc = scipy.stats.norm(angle_error, R2).pdf(np.abs(measured_angles[i]))
        #if np.any(calc):
            #weights *= calc
            #weights /= sum(weights)

    weights += 1.e-300      # avoid round-off to zero
    weights /= sum(weights) # normalize


def estimate(particles, weights):
    """returns mean and variance of the weighted particles"""
    # Out of the updated particles, calculate an estimated, most likely position
    # For Position:
    pose = particles[:, 0:4] # all positions (slice of the first four rows)
    # for np.average see https://numpy.org/doc/stable/reference/generated/numpy.average.html
    mean_pose = np.average(pose, weights=weights, axis=0) # calculate the mean of the position but with the updated weights! 
    var_pose = np.average((pose - mean_pose)**2, weights=weights, axis=0) # calculate the variance but with the updated weights! 
    # For Rotation
    return mean_pose, var_pose


def resample_from_index(particles, weights, indexes):
    # This implements a Sampling Importance Resampling filter short SIR-Filter
    particles[:] = particles[indexes]
    weights.resize(len(particles))
    weights.fill (1.0 / len(weights))
    return weights


def neff(weights):
    return 1. / np.sum(np.square(weights))


class ParticleFilterEstimator(Node):

    def __init__(self):
        super().__init__('particle_filter_estimator')

        # Parameters:
        self.initial_position_known = False # Usually start with False
        self.N = 800 # Number of particles
        self.sensor_std_err = (0.05, 0.01) # Std Error of the measurements, hier auf 5cm genau

        self.count = 0
        self.timer_period = .3 # seconds
        self.create_timer(self.timer_period, self.particle_filter_iteration)
        
        # Subscribers:
        self.landmark_subscription = self.create_subscription(DatabaseMsg, 'mapping/current_db', self.landmark_update, 1)
        self.visible_objects_subscription = self.create_subscription(ObjectArray, 'segmentation/segmented_detections', self.visible_objects_update, 1)
        self.inertia_dummy_data_subscription = self.create_subscription(DummyInertiaMsg, 'inertia', self.get_dummy_inertia_update, 1)
        self.position_subscription = self.create_subscription(Odometry, 'zed2/zed_node/odom', self.get_odom_update, 1)
        # Publishers:
        self.particles_publisher = self.create_publisher(ParticlesArrayMsg, "particlefilter/particles", 1)
        self.current_position_x_publisher = self.create_publisher(Float32, "particlefilter/current_position/x", 1)
        self.current_position_y_publisher = self.create_publisher(Float32, "particlefilter/current_position/y", 1)
        self.current_position_z_publisher = self.create_publisher(Float32, "particlefilter/current_position/z", 1)
        self.current_position_theta_publisher = self.create_publisher(Float32, "particlefilter/current_position/theta", 1)

        # Variables
        self.landmarks = [] # The landmarks from the map database
        self.visible_objects = [] # The visible objects from the cameras fov
        self.xs_history = [[0.0, 0.0, 0.0, 0.0]] # Stores the last estimated points
        self.particles = [] # Particles storing tuples (x, y, z, theta)
        self.weights = np.ones(self.N) / self.N # Erstellen der Gewichtungen. Annahme am Anfang: Alle Positionen gleich wahrscheinlich also =1
        self.current_linear_velocity = [0.0, 0.0, 0.0]
        self.current_angular_velocity = [0.0, 0.0, 0.0]
        
        self.last_position = None
        self.last_orientation = None
        self.last_timestamp = None
        self.dt = 0.0
        
        self.fusion_radius = 0.5


    def get_odom_update(self, msg):
        # msg: [header, child_frame, pose[pose[position, orientation]]]
        current_position  = np.round([
            msg.pose.pose.position.x,
            msg.pose.pose.position.y,
            msg.pose.pose.position.z
        ], 3)
        
        current_orientation  = Rotation.from_quat(np.round([
            msg.pose.pose.orientation.x,
            msg.pose.pose.orientation.y,
            msg.pose.pose.orientation.z,
            msg.pose.pose.orientation.w
        ], 3)).as_euler('xyz', degrees=True)
        
        current_timestamp = msg.header.stamp
        if self.last_position is not None:
            dt = (current_timestamp.nanosec - self.last_timestamp.nanosec) * 10**-9
            self.current_linear_velocity = [
                (current_position[0] - self.last_position[0])/dt,
                (current_position[1] - self.last_position[1])/dt,
                (current_position[2] - self.last_position[2])/dt
            ]
            self.current_angular_velocity = [
                (current_orientation[0] - self.last_orientation[0])/dt,
                (current_orientation[1] - self.last_orientation[1])/dt,
                (current_orientation[2] - self.last_orientation[2])/dt
            ]
            self.dt = dt
            #self.get_logger().info('{}'.format(dt))
            #self.get_logger().info('{}'.format(self.current_linear_velocity))
            #self.get_logger().info('{}'.format(self.current_angular_velocity))
            
        self.last_position = current_position
        self.last_orientation = current_orientation
        self.last_timestamp = current_timestamp
        pass


    def get_dummy_inertia_update(self, msg):
        self.current_linear_velocity = [
            float(msg.linear_velocity_x),
            float(msg.linear_velocity_y),
            float(msg.linear_velocity_z)
        ]
        self.current_angular_velocity = [float(msg.angular_velocity)]*3
        pass


    def landmark_update(self, database):
        # for now, we only use the x and y position and the objectclass of the landmarks
        landmarks = []
        for item in database.objects:
            landmarks.append([float(item.position[0]), float(item.position[1]), float(item.position[2]), item.objectclass, item.dbid, item.bb3d])

        #self.get_logger().info('Landmarks: {}'.format(landmarks))
        self.landmarks = np.array(landmarks)
        pass
    

    def visible_objects_update(self, msg):
        # Update the visible objects
        self.visible_objects = msg.objects
        pass


    def particle_filter_iteration(self):
        # Run the particle filter algorithm

        if self.landmarks != [] and self.current_linear_velocity is not None: # If the landmarks were updated at least once!  
            self.NL = len(self.landmarks)

            if self.count == 0:
                # create particles and weights
                if self.initial_position_known:
                    self.particles = create_gaussian_particles(mean=[0.0, 0.0, 0.0, 0.0], std=(5, 5, np.pi/4), N=self.N) # not working atm
                else:
                    self.get_logger().info('## Landmarks ##\n{}'.format(self.landmarks))
                    expansion_factor = 1.0 # amount of the box around the landmarks map
                    self.particles = create_uniform_particles(
                        x_range=(-np.amax(self.landmarks[:,0].astype(np.float32))*expansion_factor, np.amax(self.landmarks[:,0].astype(np.float32))*expansion_factor),
                        y_range=(-np.amax(self.landmarks[:,1].astype(np.float32))*expansion_factor, np.amax(self.landmarks[:,1].astype(np.float32))*expansion_factor),
                        z_range=(-np.amax(self.landmarks[:,2].astype(np.float32))*expansion_factor*2, np.amax(self.landmarks[:,2].astype(np.float32))*expansion_factor*2),
                        hdg_range=(-np.pi, np.pi),
                        N=self.N
                    )

            #self.robot_pos = self.last_pos
            # Movement from last known position
            #movement = norm(self.last_pos_known_to_pf, axis=1) # for now, not in use

            # Measured real distance from robot to each landmark
            """
            Hier müssen wir eigentlich die Distanzen zu allen Objekten in der Umgebung berechnen.
            Das ist allerdings nicht möglich, da unser FOV die sichtbaren Objekte einschränkt.
            Was also tun wir mit den Objekten, die wir nicht eindeutig zuordnen können?
            Für den Anfang setzen wir deren Distanzen auf Unendlich.
            Da wir auch keine Instanzierung durchführen können, müssen wir alle gleichen Objekte als Möglichkeit heranziehen.
            Dafür setzen wir für alle Objekte mit der selben Objectclass den selben Distanzwert.
            """

            seen_distances = self.get_seen_distances(self.landmarks, self.visible_objects)
            # Add noise model to measurement
            measured_real_distances = seen_distances #+ (randn(seen_distances.shape[0], seen_distances.shape[1]) * self.sensor_std_err[0]) 
            
            #self.get_logger().info('Landmark Positions {}'.format(self.landmarks[:,0:3].astype(np.float32)))
            
            seen_angles = self.get_seen_angles(self.landmarks, self.visible_objects)
            # Add noise model to measurement
            measured_real_angles = seen_angles #+ (randn(seen_angles.shape[0]) * self.sensor_std_err[1])

            #self.get_logger().info('## Seen distances ##\n {}'.format(measured_real_distances))
            #self.get_logger().info('## Seen angles ##\n{}'.format(np.rad2deg(seen_angles)))
            #self.get_logger().info('Real angles: {}'.format(measured_real_angles))
            

            # Berechne die Prior-Verteilung
            predict(self.particles, u=self.get_velocity_array(), std=(.08, .08), dt=self.dt)
            
            #self.get_logger().info('Particles {}'.format(self.particles))

            # Berechne die Posterior-Verteilung
            update(self.particles, self.weights, measured_distances=measured_real_distances, R1=0.2, 
                landmarks=self.landmarks, measured_angles=measured_real_angles, R2=0.2)

            # resample if too few effective particles
            if neff(self.weights) < self.N/2:
                indexes = systematic_resample(self.weights)
                self.weights = resample_from_index(self.particles, self.weights, indexes)
                assert np.allclose(self.weights, 1/self.N)

            # Get the weighted mean of the particles
            mean_pose, var_pose = estimate(self.particles, self.weights)
            #self.get_logger().info('Mean of particles: {} (x, y, z, theta)'.format(mean_pose))

            self.xs_history.append(mean_pose)
            
            # Publish the informations
            self.publish_particles(self.particles)
            f = Float32(); f.data = mean_pose[0]
            self.current_position_x_publisher.publish(f)
            f = Float32(); f.data = mean_pose[1]
            self.current_position_y_publisher.publish(f)
            f = Float32(); f.data = mean_pose[2]
            self.current_position_z_publisher.publish(f)
            f = Float32(); f.data = mean_pose[3]
            self.current_position_theta_publisher.publish(f)

            self.count += 1
        pass

    def get_velocity_array(self):
        ret = self.current_linear_velocity
        ret.append(self.current_angular_velocity[2])
        return ret


    def publish_particles(self, particles):
        # Publishes the particles as a ROS2 message

        particles_msg = ParticlesArrayMsg()
        h = Header()
        h.stamp = self.get_clock().now().to_msg()
        h.frame_id = 'map'
        particles_msg.header = h
        for p in particles:
            msg = ParticleMsg()
            msg.position = p[0:3].astype(np.float32)
            msg.theta = float(p[3])
            particles_msg.particles.append(msg) 

        self.particles_publisher.publish(particles_msg)
        pass


    def get_seen_distances(self, landmarks, visible_objects):
        ret = [[np.inf]*3]*len(landmarks)

        # We can match the instances via the tracking id which is applied before the segmentation step!
        for obj in visible_objects:
            matched_idx = self.data_association(obj)
            if matched_idx != -1:
                self.get_logger().info('Found match: {}'.format(matched_idx))
                object_visible_distances = obj.position3d[0:3]
                ret[matched_idx] = object_visible_distances
    

        """ Old instance matching

        unique_visible_object_class_names = []

        for item in visible_objects:
            if list(map(lambda x: x.objectclass, visible_objects)).count(item.objectclass) == 1:
                unique_visible_object_class_names.append(item.objectclass)

        for visibleObj in visible_objects:
            visible_objectclass = visibleObj.objectclass
            #self.get_logger().info('Visible Object:  {}'.format(visible_objectclass))
            if visible_objectclass in unique_visible_object_class_names: # Wenn eindeutige Instanz
                visible_distances = visibleObj.position3d[0:3]

                for i, marker in enumerate(landmarks):
                    # Marker related variables
                    marker_objectclass = marker[3]
                    #self.get_logger().info('Marker Object:  {}'.format(marker_objectclass))
                    if marker_objectclass == visible_objectclass:
                        if ret[i] == [np.inf]*3:
                            ret[i] = visible_distances
        """
        
        #self.get_logger().info('Calculated Distance:  {}'.format(ret))
        return np.array(ret)

    
    def get_seen_angles(self, landmarks, visible_objects):
        ret = [np.inf]*len(landmarks)

        # We can match the instances via the tracking id which is applied before the segmentation step!
        for object in visible_objects:
            matched_idx = self.data_association(object)
            if matched_idx != -1:
                self.get_logger().info('Found match: {}'.format(matched_idx))
                object_visible_angle = np.arctan2(object.position3d[1], object.position3d[0])
                ret[matched_idx] = object_visible_angle
        
        """ Old instance matching

        unique_visible_object_class_names = []

        for item in visible_objects:
            if list(map(lambda x: x.objectclass, visible_objects)).count(item.objectclass) == 1:
                unique_visible_object_class_names.append(item.objectclass)

        for visibleObj in visible_objects:
            visible_objectclass = visibleObj.objectclass
            #self.get_logger().info('Visible Object:  {}'.format(visible_objectclass))
            if visible_objectclass in unique_visible_object_class_names: # Wenn eindeutige Instanz
                visible_angle = np.arctan2(visibleObj.position3d[1], visibleObj.position3d[0])
                # This delivers an angle between -pi to +pi
                
                for i, marker in enumerate(landmarks):
                    # Marker related variables
                    marker_objectclass = marker[3]
                    #self.get_logger().info('Marker Object:  {}'.format(marker_objectclass))
                    if marker_objectclass == visible_objectclass:
                        if ret[i] == np.inf: # if no correlation was already done. Maybe insert tracking id here.
                            ret[i] = visible_angle
        """
        
        #self.get_logger().info('Calculated Angles:  {}'.format(ret))
        return np.array(ret)


    def data_association(self, obj):
        """
        ## Taken from the database_manager.py ## 

        The detection 'obj' is compared to each object in self.object_database. 
        If both have the same class and form and are close to each other in space 
        (or, preferrably, they have the same trackingid and are close in space), then they
        are associated with each other.

        If these conditions are not fulfilled for any object in self.object_database, -1 is returned.

        :param obj: the detected object.

        :returns: the index of the object in object_database associated with obj, or -1 if there is no such object.
        """
        
        
        
        min_da_iou = 0.01

        if len(self.landmarks) == 0:
            return -1
        else:
            # Use the id of the object tracked over time to associate it with
            # an object in the object_database.
            # Use the object class (not tracked over time) and
            # the object's overlap with the ones
            # in the object_database to associate the two.
            association_candidates = []

            for num, item in enumerate(self.landmarks):
                #self.get_logger().info('{}'.format(self.last_position))
                # Its not possible to use the direct odometry position here.
                # If you do so, the camera creates its frame at the 0, 0, 0 position and the correct 
                # localisation from the pf is shifted to 0, 0, 0
                # Using the xs_history instead can cause bugs especially in the kidnapped robot problem
                # This has to be fixed in the future!
                diff_x = float(item[0]) - float(obj.position3d[0] + self.xs_history[-1][0])
                diff_y = float(item[1]) - float(obj.position3d[1] + self.xs_history[-1][1])
                diff_z = float(item[2]) - float(obj.position3d[2] + self.xs_history[-1][2])
                dist = np.sqrt(diff_x**2 + diff_y**2 + diff_z**2)
                #self.get_logger().info('{}-{} ## [DA] Calc distance: {}'.format(obj.objectclass, item[3], dist))
                
                if (obj.objectclass == item[3]) and (dist < self.fusion_radius):
                        return num
                    
                elif obj.objectclass == item[3] and dist < 3*self.fusion_radius:
                    #self.get_logger().info('Calculating iou')

                    try:
                        intersection_vol, iou = box3d_overlap(
                            self.convert_bbox_points_to_array(obj.bb3d), 
                            self.convert_bbox_points_to_array(item[5])
                        )
                    except ValueError as error:
                        #print('Caught an error during iou calculation: {}'.format(error))
                        iou = 0.0

                    #self.get_logger().info('IOU: {}'.format(str(float(iou))))

                    if float(iou) > min_da_iou:
                        association_candidates.append([num, iou])

            # Associate the object with the one from the object_database that has the highest overlap, 
            # or with no object if it overlaps with no object from the object_database.
            iou_max = 0
            best_candidate = -1
                
            if len(association_candidates) != 0:
                for item in association_candidates:
                    if item[1] > iou_max:
                        iou_max = item[1]
                        best_candidate = item[0]
                self.get_logger().info('{} with iou {}'.format(best_candidate, iou_max))
                return best_candidate # idx
            else:
                return -1

    
    def convert_bbox_points_to_array(self, box):
        #self.get_logger().info('box: {}'.format(box))
        ret = []
        for point in box:
            ret.append([point.x, point.y, point.z])

        ret = np.array(ret).astype(np.float32)
        ret = torch.from_numpy(np.resize(ret, (1, 8, 3)))
        #self.get_logger().info('output: {}, shape: {}'.format(ret, ret.shape))
        return ret


def main(args=None):
    rclpy.init(args=args)
    node = ParticleFilterEstimator()
    rclpy.spin(node)
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()

if __name__=="__main__":
    main()