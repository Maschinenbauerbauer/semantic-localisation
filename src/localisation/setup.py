from setuptools import setup
from glob import glob
import os

package_name = 'localisation'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*.launch.py'))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='andreas',
    maintainer_email='andi.bauer9716@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'local_localisation = localisation.local_localisation:main',
            'virtual_frame = localisation.virtual_frame:main',
            'particle_filter_2d = localisation.particle_filter_2d:main',
            'particle_filter_3d = localisation.particle_filter_3d:main',
            'particle_filter_3d_visualizer = localisation.particle_filter_3d_visualizer:main',
            'trilateration_2d = localisation.trilateration_2d:main',
            'tester = localisation.tester:main'
        ],
    },
)
