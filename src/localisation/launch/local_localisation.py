import launch
import launch.actions
import launch.substitutions
import launch_ros.actions


def generate_launch_description():
    return launch.LaunchDescription([
        launch.actions.DeclareLaunchArgument(
            'node_prefix',
            default_value=[launch.substitutions.EnvironmentVariable('USER'), '_'],
            description='Prefix for node names'),
        launch_ros.actions.Node(
            package='localisation', executable='virtual_frame', output='screen',
            name=[launch.substitutions.LaunchConfiguration('node_prefix'), 'virtual_frame']),
        launch_ros.actions.Node(
            package='localisation', executable='local_localisation', output='screen',
            name=[launch.substitutions.LaunchConfiguration('node_prefix'), 'pose_estimator']),
    ])