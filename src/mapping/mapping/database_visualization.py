#!/usr/bin/env python
from __future__ import division

import time
import rclpy
from rclpy.node import Node
from rclpy.time import Duration

from msgs.msg import DatabaseMsg

from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Point

class Database_Viz(Node):

    def __init__(self):
        super().__init__('viz_map')
        self.sub_db = self.create_subscription(DatabaseMsg, "mapping/current_db",
                                       self.callback_display_database, 
                                       1)   
        
        self.pub_marker = self.create_publisher(MarkerArray, "mapping_viz/db_markers", 1)
        self.pub_centroid = self.create_publisher(MarkerArray, "mapping_viz/centroid_markers", 1)

        # Necessary to delete all markers on new startup!
        marker_array = MarkerArray()
        marker = Marker()
        marker.id = 0
        
        marker.action = marker.DELETEALL
        marker_array.markers.append(marker)
        self.pub_marker.publish(marker_array)
        self.pub_centroid.publish(marker_array)

        
    def callback_display_database(self, DatabaseMsg):
        
        marker_array = MarkerArray()
        centroid_marker_array = MarkerArray()
        
        counter = 0
        counter_centroids = 0
        for item in DatabaseMsg.objects:
            c = False
            if item.isdynamic:
                #marker = self.line(DatabaseMsg.header, item.positionhistory, counter)
                #marker_array.markers.append(marker)
                counter += 1
                c = True

            marker = self.generate_marker_cube_skeleton(DatabaseMsg.header, item, counter, c)
            marker_array.markers.append(marker)
            counter += 1
            
            marker = self.generate_marker_text(DatabaseMsg.header, item, counter, c)
            marker_array.markers.append(marker)
            counter += 1

            centroid_marker = self.generate_centroid_markers(DatabaseMsg.header, item, counter_centroids, c)
            centroid_marker_array.markers.append(centroid_marker)
            counter_centroids += 1

        self.pub_marker.publish(marker_array)
        self.pub_centroid.publish(centroid_marker_array)

    def generate_centroid_markers(self, header, obj, num, c):
        msg = Marker()
        msg.header = header
        msg.type = msg.SPHERE
        msg.action = msg.ADD
        msg.id = num
        msg.lifetime = Duration(seconds=.2).to_msg()

        msg.scale.x = .07
        msg.scale.y = .07
        msg.scale.z = .07

        msg.color.a = 1.0
        msg.color.r = 0.0
        msg.color.g = 0.0
        msg.color.b = 1.0
        if c:
            msg.color.a = 1.0
            msg.color.r = 1.0
            msg.color.g = 0.0
            msg.color.b = 0.0

        msg.pose.position.x, msg.pose.position.y, msg.pose.position.z  = float(obj.position[0]), float(obj.position[1]), float(obj.position[2])
        msg.pose.orientation.x = msg.pose.orientation.y = msg.pose.orientation.z = 0.0
        msg.pose.orientation.w = 1.0

        return msg

        

    
    def generate_marker_cube_skeleton(self, header, obj, num, c):
        """
        Generates a marker cube made out of lines so that the object within remains visible.

        :param header: The header.
        :param obj: The object.
        :param num: The ID.
        :param scale: The line-width.
        :return: A ROS message with the LINE_LIST that makes up the cube-skeleton around the object.
        """
        msg = Marker()
        msg.header = header
        self.get_logger().info(str(msg.header))
        msg.type = msg.LINE_LIST
        msg.action = msg.ADD
        msg.id = num
        msg.lifetime = Duration(seconds=.2).to_msg()

        msg.scale.x = .01
        msg.scale.y = .01
        msg.scale.z = .01

        msg.color.a = 1.0
        msg.color.r = 1.0
        msg.color.g = 0.0
        msg.color.b = 0.0
        if c:
            msg.color.a = 1.0
            msg.color.r = 1.0
            msg.color.g = 0.0
            msg.color.b = 0.0

        # Shape of skeleton:
        #    7 -------- 6
        #   /|         /|
        #  / |        / |
        # 4 -------- 5  |
        # |  |       |  |
        # |  3 ------|- 2        z
        # | /        | /         |  y
        # |/         |/          | /
        # 0 -------- 1           |/____ x

        # Create Points:
        bb3d_py = list(obj.bb3d)
        self.get_logger().info(str(obj.bb3d))
        p0 = bb3d_py[0]
        p1 = bb3d_py[1]
        p2 = bb3d_py[2]
        p3 = bb3d_py[3]
        p4 = bb3d_py[4]
        p5 = bb3d_py[5]
        p6 = bb3d_py[6]
        p7 = bb3d_py[7]
        
        # Add edges to msg:
        # Point pair for each edge to draw
        msg.points.append(p0)  
        msg.points.append(p1) 

        msg.points.append(p1)  
        msg.points.append(p2) 
        
        msg.points.append(p2)  
        msg.points.append(p3) 

        msg.points.append(p3)  
        msg.points.append(p0) 
        
        msg.points.append(p0)  
        msg.points.append(p4)  
        
        msg.points.append(p1)  
        msg.points.append(p5)  

        msg.points.append(p2)  
        msg.points.append(p6)  

        msg.points.append(p3)
        msg.points.append(p7)

        msg.points.append(p4)
        msg.points.append(p5)

        msg.points.append(p5)
        msg.points.append(p6)

        msg.points.append(p6)
        msg.points.append(p7)

        msg.points.append(p7)
        msg.points.append(p4)

        return msg
    
    def generate_marker_text(self, header, item, num, c):
        msg = Marker()
        msg.header = header
        msg.type = msg.TEXT_VIEW_FACING
        cls = item.objectclass
        msg.text = cls
        msg.action = msg.ADD
        msg.id = num
        msg.lifetime = Duration(seconds=0.5).to_msg()
        msg.pose.position.x = item.position[0] + (item.bb3d[0].x - item.bb3d[1].x)/2# + 0.3
        msg.pose.position.y = item.position[1] - (item.bb3d[0].y - item.bb3d[4].y)/2# - 0.5
        msg.pose.position.z = item.position[2] + (item.bb3d[0].z - item.bb3d[3].z)/2# + 0.1 
        msg.pose.orientation.x = 0.
        msg.pose.orientation.y = 0.
        msg.pose.orientation.z = 0.
        msg.pose.orientation.w = 1.
        msg.scale.z = 0.05
        msg.color.a = 1.0
        msg.color.r = 1.0
        msg.color.g = 0.0
        msg.color.b = 0.0

        if c:
            msg.color.a = 1.0
            msg.color.r = 1.0
            msg.color.g = 0.0
            msg.color.b = 0.0     
        return msg    
    
    def line(self, header, pos, num):
        msg = Marker()
        msg.header = header
        msg.type = msg.LINE_STRIP
        msg.action = msg.ADD
        msg.id = num
        msg.lifetime = Duration(seconds=0.2).to_msg()
        msg.pose.position.x = 0.0
        msg.pose.position.y = 0.0
        msg.pose.position.z = 0.0
        msg.pose.orientation.x = 0.
        msg.pose.orientation.y = 0.
        msg.pose.orientation.z = 0.
        msg.pose.orientation.w = 1.
        msg.scale.x = 0.05
        msg.scale.y = 0.05
        msg.scale.z = 0.05
        msg.color.a = 1.0
        msg.color.r = 1.0
        msg.color.g = 0.0
        msg.color.b = 0.0 
        msg.points = []
        self.get_logger().info('Here.')
        if len(pos) > 3:
            for i,p in enumerate(pos):
                if i % 3 == 0:
                    first_line_point = Point(pos[i], pos[i+1], pos[i+2])
                    msg.points.append(first_line_point)
            return msg
    
def main(args=None):
    print("DataBase Viz started")     

    rclpy.init(args=args)
    node = Database_Viz()
    rclpy.spin(node)

    time.sleep(2)
    node.destroy_node()
    rclpy.shutdown()