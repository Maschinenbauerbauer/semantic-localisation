#!/usr/bin/env python


import mapping.PersistenceFilter as pf
import mapping.persistence_filter_utils as pf_utils
#import rospy

class Object():
    def __init__(self, dbid, objectclass, classconf, isdynamic, \
                 trackingid, position, positionhistory, firstdetection,  updatehistory, \
                 velocity, bb3d, \
                 P_M=0.2, P_F=0.25):
        """
        Initialize the attributes.

        :param dbid: The database ID.
        :param semantics: The class of the object. Called type because both class and cls are Python built-ins.
        :param trackingid: the ID that detection_tracking gave the object.
        :param position: The position.
        :param positionhistory: The position history.
        :param bb3d: The 3D-boundingbox.
        :param not_det: 'Not found'; Used when an object that is in the database and should be visible on screen is not detected in '/segmented_detections'.
        :param last_det: The time of the last detection.
        :param tof_app: The time the object was first detected.
        """
        self.dbid = dbid
        self.objectclass = objectclass
        self.classconf = classconf
        self.isdynamic = isdynamic # isdynamic # CHANGE
        self.trackingid = trackingid
        self.position = position
        self.positionhistory = positionhistory
        self.updatehistory = updatehistory
        self.firstdetection = firstdetection
        self.velocity = velocity
        self.bb3d = bb3d

        # Error probabilities:
        self.P_M = P_M        # Missed detection probability
        self.P_F = P_F        # False alarm probability
        
        #self.lambda_l = 0.00000001
        #self.lambda_u = 0.00000002
        self.lambda_c = 0.01 # 0.001 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        #CHANGE
        if self.objectclass == 'person':
            self.isdynamic = True

        if self.isdynamic:
            #self.lambda_l = 1.0
            #self.lambda_u = 2.0
            self.lambda_c = 1.0
            
        self.confidence = 1.0

        self._logS_T = lambda t: -self.lambda_c*t
        #self._logS_T = lambda t: pf_utils.log_general_purpose_survival_function(t, self.lambda_l, self.lambda_u)
        
        self.persistence_filter = pf.PersistenceFilter(self._logS_T)

    def pf_predict(self, d_t):
        """Initialize the confidence value using the persistence filter."""
        self.confidence = self.persistence_filter.predict(d_t)
        print('Confidence returned: {}'.format(self.confidence))

    def pf_update(self, was_detected, d_t):
        """
        Update the confidence using the persistence filter.

        :param was_detected: True if the item was detected, False if it should have been detected but wasn't.
        :returns: The current confidence.
        """
        # Update confidence:
        self.persistence_filter.update(was_detected, d_t, self.P_M, self.P_F)
        self.confidence = self.persistence_filter.predict(d_t)
