#!/usr/bin/env python
from __future__ import division

import numpy as np
import math
import time


import rclpy
from rclpy.node import Node
from rclpy.duration import Duration
from rclpy.time import Time

import torch
from pytorch3d.ops import box3d_overlap


import tf2_ros as tf
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
import tf2_geometry_msgs
from tf2_ros import LookupException, ConnectivityException, ExtrapolationException

from copy import copy

import mapping.c_object as c_object

import image_geometry
from sensor_msgs.msg import CameraInfo
from geometry_msgs.msg import PointStamped, Vector3Stamped
from msgs.msg import ObjectArray
from msgs.msg import DatabaseMsg, DatabaseObject

# CHANGE:
import cv2
cv2_debugging = False

#from people_msgs.msg import People, Person
#from desk_msgs.msg import Desk, DeskArray

class Database_Manager(Node):
    def __init__(self):
        super().__init__('database_manager')

        self.get_logger().info("DataBase Manager started")

        # TF2
        self.tfBuffer = Buffer()
        print('Buffer created')
        self.tf_listener = TransformListener(self.tfBuffer, self)
        print('TransformListener created')
        
        time.sleep(5.0)

        self.camera_info = None # TODO: This is not perfect, but as for now, there is no wait for message in ROS2

        # Get the camera information for reprojection purposes
        self.camera_info_sub = self.create_subscription(
            CameraInfo,
            "/zed2/zed_node/left_raw/camera_info",
            self.get_camera_info,
            10
        )
        
        # Subscriber to the segementation node output
        # For contents, see src/msgs/msg/ObjectArray.msg
        self.sub = self.create_subscription(
            ObjectArray,
            "segmentation/segmented_detections",
            self.callback_manage_database,
            1
        )

        # Publishes current_db information
        self.pub_db = self.create_publisher(DatabaseMsg, "mapping/current_db", 1)
        #self.pub_person = self.create_publisher(People, "/people", 1)
        #self.pub_desk = self.create_publisher(DeskArray, "/desk_list", 1
        
        self.camera_model = image_geometry.PinholeCameraModel()

        self.object_database = [] # classes in it are of type c_object.Object()
        self.dbid = 1
        self.min_da_iou = 0.01   # Minimum iou for data_association
        self.forget_objects = False
        self.time_drop = False
        self.fusion_radius = 0.5
        self.current_db_time = 0
        

    def get_camera_info(self, msg):
        self.camera_info = msg
        self.camera_model.fromCameraInfo(self.camera_info)


    def callback_manage_database(self, ObjectArray_msg):
        
        try:
            detected_objects = []
            self.current_db_time = ObjectArray_msg.header.stamp
            #self.get_logger().info('Callback')
            
            for obj_idx, obj in enumerate(ObjectArray_msg.objects): 
                if (obj.position3d[0] != 0 and obj.position3d[1] != 0 and obj.position3d[2] != 0):
                    #self.get_logger().info('{} | {}'.format(obj.position3d, obj.bb3d))
                    new_position3d, new_bb3d = self.transform_to_map_coordinates(obj.position3d, obj.bb3d, ObjectArray_msg)
                    #self.get_logger().info('new_bb3d {}'.format(new_bb3d))
                    ObjectArray_msg.objects[obj_idx].position3d = new_position3d
                    ObjectArray_msg.objects[obj_idx].bb3d = new_bb3d
                    #self.get_logger().info('{} | {}'.format(obj.position3d, obj.bb3d))
                    
                    # Is obj already in self.object_database?
                    # idx: index of object from object_database associated with obj.
                    idx = self.data_association(obj)
                    #self.get_logger().info('{} | {}'.format(obj.position3d, obj.bb3d))
                    # Update self.object_database accordingly.
                    self.update_database(idx, ObjectArray_msg.objects[obj_idx])

                    # Update the list of objects from ObjectArray_msg to later 
                    # compare it to the objects form the
                    # object_database that should be detected by the camera.
                    detected_objects.append(self.object_database[idx].dbid)

            if self.time_drop:
                # Drop items, that are not detected for a specific time
                for idx, item in enumerate(self.object_database):
                    if ((self.current_db_time.sec - item.updatehistory[-1].sec) > 3.0):
                        #print("Dropping {}".format(item.objectclass))
                        self.object_database.pop(idx)
                        #print("Objects in DB: "+str(len(self.object_database)))

                    
            if (self.forget_objects == True):
                
                expected_objects = self.check_fov()
                self.get_logger().info('Expected objects: ' + str(expected_objects)) 

                missing_objects = []
                for item in expected_objects:
                    if item not in detected_objects:
                        missing_objects.append(item)

                self.get_logger().info('Missing objects: ' + str(missing_objects)) if missing_objects != [] else None

                new_db = []
                
                for item in self.object_database:
                    #self.get_logger().info('time diff:' + str((self.current_db_time.sec - item.firstdetection.sec)))
                    if (self.current_db_time.sec - item.firstdetection.sec) > 0.0:
                        if item.dbid in missing_objects:
                            self.get_logger().info('dbid in missing objects')
                            item.pf_update(False, self.current_db_time.sec - item.firstdetection.sec)
                        elif item.dbid in expected_objects:
                            self.get_logger().info('dbid in expected objects')
                            item.pf_update(True, self.current_db_time.sec - item.firstdetection.sec)
                        else: #item.isdynamic:
                            self.get_logger().info('item is updated')
                            item.pf_predict(self.current_db_time.sec - item.firstdetection.sec)

                        
                        self.get_logger().info('Confidence: {}, {}'.format(item.confidence, item.objectclass))  if item.confidence != 1.0 else None

                        if (item.confidence >= 0.5):
                            new_db.append(item)
                    else:
                        new_db.append(item)
                        
                self.object_database =  new_db

            self.output_result()
        except (LookupException, ConnectivityException, ExtrapolationException):
            self.get_logger().info('transform not ready')
            return
                     
    def data_association(self, obj):
        """
        The detection 'obj' is compared to each object in self.object_database. 
        If both have the same class and form and are close to each other in space 
        (or, preferrably, they have the same trackingid and are close in space), then they
        are associated with each other.

        If these conditions are not fulfilled for any object in self.object_database, -1 is returned.

        :param obj: the detected object.

        :returns: the index of the object in object_database associated with obj, or -1 if there is no such object.
        """
        
        # TODO: Apply changes which come from using all Points for BoundingBox

        if len(self.object_database) == 0:
            return -1
        else:
            # Use the id of the object tracked over time to associate it with 
            #an object in the object_database.
            # Use the object class (not tracked over time) and 
            # the object's overlap with the ones 
            # in the object_database to associate the two.
            association_candidates = []

            for num, item in enumerate(self.object_database): 
                
                diff_x = item.position[0] - obj.position3d[0]
                diff_y = item.position[1] - obj.position3d[1]
                diff_z = item.position[2] - obj.position3d[2]
                dist = np.sqrt(diff_x**2 + diff_y**2 + diff_z**2)
                
                if (obj.trackingid != 0) and \
                   (obj.trackingid == item.trackingid) and \
                   (dist < self.fusion_radius):
                        return num
                    
                elif (obj.objectclass == item.objectclass) and \
                     (dist < self.fusion_radius):
                       
                    # Get the centers of obj and item aligned so that iou 
                    # compares the form of the objects, 
                    # not the position (which is already done 
                    # with the dist < 1.5 comparison).
                    """
                    bb3_db = obj.bb3d
                    bb3_db[0][0] += diff_x
                    bb3_db[0][1] += diff_y
                    bb3_db[0][1] += diff_z
                    bb3_db[1][0] += diff_x
                    bb3_db[1][1] += diff_y
                    bb3_db[1][1] += diff_z
                    bb3_db[2][0] += diff_x
                    bb3_db[2][1] += diff_y
                    bb3_db[2][1] += diff_z
                    
                    #bb3_db[0] += diff_x
                    #bb3_db[1] += diff_y
                    #bb3_db[2] += diff_z
                    #bb3_db[3] += diff_x
                    #bb3_db[4] += diff_y
                    #bb3_db[5] += diff_z
                    
                    iou = self.iou_3d(item.bb3d, bb3_db)
                    """
                    self.get_logger().info('Calculating iou')
                    try:
                        intersection_vol, iou = box3d_overlap(
                            self.convert_bbox_points_to_array(obj.bb3d), 
                            self.convert_bbox_points_to_array(item.bb3d)
                        )
                    except ValueError as error:
                        print('Caught an error during iou calculation: {}'.format(error))
                        iou = 0.0

                    self.get_logger().info('IOU: {}'.format(str(float(iou))))

                    if float(iou) > self.min_da_iou:
                        association_candidates.append([num, iou])

            # Associate the object with the one from the object_database that has the highest overlap, 
            # or with no object if it overlaps with no object from the object_database.
            iou_max = 0
            best_candidate = -1
                
            if len(association_candidates) != 0:
                for item in association_candidates:
                    if item[1] > iou_max:
                        iou_max = item[1]
                        best_candidate = item[0]
                return best_candidate
            else:
                return -1

    def convert_bbox_points_to_array(self, box):
        #self.get_logger().info('box: {}'.format(box))
        ret = []
        for point in box:
            ret.append([point.x, point.y, point.z])

        ret = np.array(ret).astype(np.float32)
        ret = torch.from_numpy(np.resize(ret, (1, 8, 3)))
        self.get_logger().info('output: {}, shape: {}'.format(ret, ret.shape))
        return ret


    def update_database(self, idx, obj): 
        """
        Add a new object to the object_database if idx == -1 or else update the existing object corresponding to idx.

        :param idx: The index of the detected object obj in self.object_database. If obj is not associated with any entry in self.object_database, idx == -1.
        :param obj: The detected object with which the database is being updated.
        :param time_of_detection: The time at which obj was detected.
        """

        if idx == -1:
            new_object = c_object.Object(
                dbid=self.dbid, 
                objectclass = obj.objectclass,
                classconf = obj.confidence,
                isdynamic = obj.isdynamic,
                trackingid = obj.trackingid,
                position = obj.position3d, 
                positionhistory = [], 
                updatehistory = [self.current_db_time], 
                firstdetection = self.current_db_time,
                velocity = [0., 0., 0.],
                bb3d = obj.bb3d
            )
            self.object_database.append(new_object)
            self.dbid += 1
            #print("Objects in DB: "+str(len(self.object_database)))
            
        else:  
            
            if obj.trackingid != 0:
                self.object_database[idx].trackingid = obj.trackingid
            
            if len(self.object_database[idx].positionhistory) > 300:
                self.object_database[idx].positionhistory.pop(0)
                self.object_database[idx].updatehistory.pop(0)
                
            self.object_database[idx].positionhistory.append(self.object_database[idx].position)
            self.object_database[idx].updatehistory.append(self.current_db_time)
            
            weight_factor = len(self.object_database[idx].updatehistory)
            
            if obj.confidence > self.object_database[idx].classconf:
                    self.object_database[idx].objectclass = obj.objectclass
                    self.object_database[idx].classconf = obj.confidence

            # If the object is dynamic, it moves and its center is supposed to 
            # change. If it is not, then any change in the center of mass of 
            # the object is due to measuring errors. These should be 
            # compensated by averaging the center and weighing the previously 
            # determined center by the amount of measurements that went into 
            # it (weight_factor).

            if obj.isdynamic:
                self.object_database[idx].isdynamic = True 
                self.object_database[idx].position = obj.position3d
                self.object_database[idx].velocity = [0., 0., 0.]
                '''
                d_v_x = []
                d_v_y = []
                d_v_z = []
                
                for num, position in enumerate(self.object_database[idx].positionhistory):
                    if num > 4:
                        break
                    elif num > 0:
                        d_v_x.append((position[0] - self.object_database[idx].positionhistory[num-1][0]) \
                        / (self.object_database[idx].updatehistory[num].to_sec() - self.object_database[idx].updatehistory[num-1].to_sec()))

                        d_v_y.append((position[1] - self.object_database[idx].positionhistory[num-1][1]) \
                        / (self.object_database[idx].updatehistory[num].to_sec() - self.object_database[idx].updatehistory[num-1].to_sec()))
                        
                        d_v_z.append((position[2] - self.object_database[idx].positionhistory[num-1][2]) \
                        / (self.object_database[idx].updatehistory[num].to_sec() - self.object_database[idx].updatehistory[num-1].to_sec()))

                if len(d_v_x) > 0:
                    d_v_x_a = sum(d_v_x) / len(d_v_x)
                    d_v_y_a = sum(d_v_y) / len(d_v_y)
                    d_v_z_a = sum(d_v_z) / len(d_v_z)
                    self.object_database[idx].velocity = [d_v_x_a, d_v_y_a, d_v_z_a]
                '''
            else:
                self.object_database[idx].isdynamic = False
                x_o = self.object_database[idx].position[0]
                y_o = self.object_database[idx].position[1]
                z_o = self.object_database[idx].position[2]
                
                new_x = (obj.position3d[0] + x_o * weight_factor) / (weight_factor + 1)
                new_y = (obj.position3d[1] + y_o * weight_factor) / (weight_factor + 1)
                new_z = (obj.position3d[2] + z_o * weight_factor) / (weight_factor + 1)
                self.object_database[idx].position = [new_x, new_y, new_z]
                
                self.object_database[idx].velocity = [0., 0., 0.]
                
            # The object's bb in the object_database will be updated.
            # If the object has appeared often before (measured by weight), then the bb shouldn't change
            # too much; the old bb should be weighted more strongly compared to the newly measured one.
                
            cur_bb = self.object_database[idx].bb3d # cur for current = last known bounding box
            #x_o_size = abs(cur_bb[3] - cur_bb[0]) # o for old
            #y_o_size = abs(cur_bb[4] - cur_bb[1])
            #z_o_size = abs(cur_bb[5] - cur_bb[2])

            #bb3_db = obj.bb3d
            #x_n_size = abs(bb3_db[3] - bb3_db[0]) # n for new
            #y_n_size = abs(bb3_db[4] - bb3_db[1])
            #z_n_size = abs(bb3_db[5] - bb3_db[2])

            #new_s_x = (x_n_size + x_o_size * weight_factor) / (weight_factor + 1) # s for size
            #new_s_y = (y_n_size + y_o_size * weight_factor) / (weight_factor + 1)
            #new_s_z = (z_n_size + z_o_size * weight_factor) / (weight_factor + 1)
            
            #new_bb = [None] * 6
            #new_bb[0] = obj.position3d[0] - (new_s_x/2)
            #new_bb[1] = obj.position3d[1] - (new_s_y/2)
            #new_bb[2] = obj.position3d[2] - (new_s_z/2)
            #new_bb[3] = obj.position3d[0] + (new_s_x/2)
            #new_bb[4] = obj.position3d[1] + (new_s_y/2)
            #new_bb[5] = obj.position3d[2] + (new_s_z/2)
            
            self.object_database[idx].bb3d =  obj.bb3d #new_bb

    def check_fov(self): 
        """
        Compares the current camera frame to all objects in self.object_database.

        If the object is inside the camera frame, it should be detected by /segmented_detections. 
        It is therefore added to a list of expected detections.

        :returns: expected_objects, a list of objects that should be within the camera's view and should therefore be detected.
        """
        expected_objects = []
        #self.tfBuffer.wait_for_transform_async('map', 'zed2_base_link', Duration(seconds=.5))
        now = rclpy.time.Time()
        trans =  self.tfBuffer.lookup_transform('map', 'zed2_left_camera_frame', now)
                
        p_map = PointStamped()
        p_map.header.stamp = now.to_msg()
        p_map.header.frame_id = 'map'

        "Debugging with cv2"
        # CHANGE:
        img = np.zeros((720, 1280, 3)) if cv2_debugging else None
        
        for num, item in enumerate(self.object_database):

            """
            # If the items position is near the current translation of the transformation
            if (trans.transform.translation.x < item.position[0] < (trans.transform.translation.x+5.0) 
                and (trans.transform.translation.y-5.0) < item.position[1] <  (trans.transform.translation.y+5.0) 
                and (trans.transform.translation.z-5.0) < item.position[2] < (trans.transform.translation.z+5.0)
                ):
            """
            # Create a PointStamped object with the item's position 
            # and transform it from the map frame to the camera frame.
            p_map.point.x = float(item.position[0])
            p_map.point.y = float(item.position[1])
            p_map.point.z = float(item.position[2])
            #self.get_logger().info('P_map: ' + str(p_map))
            p_camera = self.tfBuffer.transform(p_map, 'zed2_left_camera_frame')

            #self.get_logger().info('P_camera: ' + str(p_camera))
            # Project the 3D information from p_map onto the pixels of the camera.

            x = -p_camera.point.y
            y = -p_camera.point.z
            z = p_camera.point.x

            self.get_logger().info('{}'.format(p_camera))
            
            p_image = self.camera_model.project3dToPixel([x, y, z])

            # If item's location is within the camera frame,
            # we should expect that the item is detected.
            
            # CHANGE:
            if cv2_debugging:
                center = (int(p_image[0]), int(p_image[1]))
                cv2.putText(img, item.objectclass, (int(p_image[0])+10, int(p_image[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1, 8)
                cv2.circle(img, center, 10, (0,255,0), thickness=1, lineType=8, shift=0)


            border = 0 # px
            if (border < p_image[0] < 1280-border) and (border < p_image[1] < 720-border) and p_camera.point.x > 0:
                # inside the 1280x720p pixel image
                expected_objects.append(self.object_database[num].dbid)

        # CHANGE:
        if cv2_debugging:
            cv2.imshow('img', img)
            cv2.waitKey(1)

        return expected_objects
        
    def output_result(self):
        """Generates a list of markers and text for each object, 
        as well as lines connecting the positions of dynamic objects,
        and publishes them to '/markers'."""
        
        current_db = DatabaseMsg()
        current_db.header.stamp = self.current_db_time
        current_db.header.frame_id = 'map'

        #msg = People()
        #msg.header.stamp = self.current_db_time
        #msg.header.frame_id = "map"

        for item in self.object_database:
            db_obj = DatabaseObject()
            db_obj.objectclass = item.objectclass
            db_obj.classconf = item.classconf
            db_obj.isdynamic = item.isdynamic
            #print(db_obj.isdynamic) if item.isdynamic else None
            db_obj.position = item.position
            #print(type(item.positionhistory))

            if len(item.positionhistory) > 0:
                for idx, element in enumerate(item.positionhistory):
                    #print(type(element))
                    if type(element) != list:
                        # Else numpy array
                        item.positionhistory[idx] = element.tolist()

            #print(item.positionhistory)
            db_obj.positionhistory = [it for sb in item.positionhistory for it in sb ]
            db_obj.velocity = item.velocity
            db_obj.trackingid = item.trackingid
            db_obj.dbid = item.dbid
            db_obj.bb3d = item.bb3d
            current_db.objects.append(db_obj)
            '''
            if item.objectclass == "person":
                person = Person()
                person.name = "Person db_id"
                person.position.x = item.position[0]
                person.position.y = item.position[1]
                person.position.z = item.position[2]
                person.velocity.x = item.velocity[0]
                person.velocity.y = item.velocity[1]
                person.velocity.z = item.velocity[2]
                person.reliability = 0.9
                person.tagnames = [""]
                person.tags = [""]
                msg.people.append(person)
            '''
        self.pub_db.publish(current_db)


    def transform_to_map_coordinates(self, position3d, bb3d, objectarray_msg):
        """
        Transform the center and boundingbox of an object from its local frame into the global map frame.

        :param position3d: The center of the object.
        :param bb3d: the boundingbox of the object.

        :returns: position3d, bb3_db, the center and boundingbox of the object in map coordinates.
        """
        #print
        local_frame = 'zed2_left_camera_frame'
        target_frame = 'map' 
        timeout = Duration(seconds=1)
        #now = self.get_clock().now() - Duration(seconds=.2)
        now = Time(seconds=objectarray_msg.header.stamp.sec, nanoseconds=objectarray_msg.header.stamp.nanosec)# - Duration(seconds=.2)
        self.get_logger().info('{}'.format(self.get_clock().now()))
        self.get_logger().info('{}'.format(objectarray_msg.header.stamp))
        self.get_logger().info('{}'.format(Time(seconds=self.get_clock().now().seconds_nanoseconds()[0], nanoseconds=self.get_clock().now().seconds_nanoseconds()[1]) - Time(seconds=objectarray_msg.header.stamp.sec, nanoseconds=objectarray_msg.header.stamp.nanosec)))
        # Block until, transform is available
        trans =  self.tfBuffer.lookup_transform(target_frame, local_frame, now)
            
        
        #self.get_logger().info(str(trans.header.stamp))

        # Position 3D
        centroid_zed2 = PointStamped()
        centroid_zed2.header.stamp = now.to_msg()
        centroid_zed2.header.frame_id = local_frame
        centroid_zed2.point.x = float(position3d[0])
        centroid_zed2.point.y = float(position3d[1])
        centroid_zed2.point.z = float(position3d[2])
        centroid_map = self.tfBuffer.transform(centroid_zed2, target_frame)
        position3d_map = [centroid_map.point.x, centroid_map.point.y, centroid_map.point.z]

        # Bounding Box 
        bb3_db_map = []
        for point in bb3d:
            p = self.transform_point(point, now, local_frame, target_frame)
            #self.get_logger().info(str(p.point))
            bb3_db_map.append(p.point)

        return position3d_map, bb3_db_map

    def transform_point(self, p, time_now, local_frame, target_frame):
        bb_point_zed2 = PointStamped()
        bb_point_zed2.header.stamp = time_now.to_msg()
        bb_point_zed2.header.frame_id = local_frame
        bb_point_zed2.point.x = float(p.x) # min x
        bb_point_zed2.point.y = float(p.y) # min y
        bb_point_zed2.point.z = float(p.z) # min z
        bb_point_map = self.tfBuffer.transform(bb_point_zed2, target_frame)
        return bb_point_map


    def iou_3d(self, A, B): 
        """Calculate the percentage overlap of the bounding boxes of two objects A and B.""" 

        # TODO: This does not work anymore! :(
                
        xA = max(A[0][0], B[0][0])
        yA = max(A[0][1], B[0][1])
        zA = max(A[0][2], B[0][2])
        xB = min(A[2][0], B[2][0])
        yB = min(A[2][1], B[2][1])
        zB = min(A[2][2], B[2][2])

        interVol = max(0, xB - xA) * max(0, yB - yA) * max(0, zB - zA)
        if interVol == 0:
            return 0.0

        boxAVol = abs(A[2][0] - A[0][0]) * abs(A[2][1] - A[0][1]) * abs(A[2][2] - A[0][2])
        boxBVol = abs(B[2][0] - B[0][0]) * abs(B[2][1] - B[0][1]) * abs(B[2][2] - B[0][2])

        iou = interVol / float(boxAVol + boxBVol - interVol)
        return iou

        
def main(args=None):
    rclpy.init(args=args)
    database_manager_node = Database_Manager()
    rclpy.spin(database_manager_node)
    database_manager_node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()


'''
#Display image with reprojected Objects:

#import
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge
import message_filters

#__init__
self.sub = message_filters.Subscriber("/segmented_detections", ObjectArray, buff_size=2**30)
self._image_sub = message_filters.Subscriber(
    "/camera/color/image_raw",T
     Image, buff_size=100
)
self._bridge = CvBridge()
self._sync = message_filters.ApproximateTimeSynchronizer(
    [self.sub, self._image_sub],
     queue_size=100, slop=0.1, allow_headerless=False
)
self._sync.registerCallback(self.callback_manage_database)
self.image = 0
'''
