from __future__ import division

import time
import rclpy
from rclpy.node import Node

from msgs.msg import DatabaseMsg

import pickle

load_database = True

class Database_Viz(Node):

    def __init__(self):
        super().__init__('save_map')

        self.database = None
        self.once = True

        if not load_database:
            self.get_logger().info('Saving current map..')
            self.sub_db = self.create_subscription(
                DatabaseMsg, "mapping/current_db",
                self.callback_database,
                1
            )   
        else:
            self.get_logger().info('Opening and publishing saved map..')
            filehandler = open(r'/home/andreas/ma_ws/src/mapping/stored_database.pkl',"rb")
            self.database = pickle.load(filehandler)
            self.timer = self.create_timer(timer_period_sec=.2, callback=self.publish_db)
            self.pub = self.create_publisher(DatabaseMsg, '/mapping/current_db', 1)

        
    def publish_db(self):
        self.pub.publish(self.database)
           
    
    def callback_database(self, msg):
        if self.once:
            self.database = msg
            filehandler = open(r'/home/andreas/ma_ws/src/mapping/stored_database.pkl',"wb")
            self.get_logger().info('Dumping database..')
            pickle.dump(self.database, filehandler)
            self.get_logger().info('Save done.')
            self.once = False

    
def main(args=None):
    print("Save Map Helper started")     

    rclpy.init(args=args)
    node = Database_Viz()
    rclpy.spin(node)

    time.sleep(2)
    node.destroy_node()
    rclpy.shutdown()