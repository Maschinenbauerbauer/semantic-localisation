# Information

This is the mapping node of the project. 
This aims to create/maintain a map of the surroundings. 
For the use case of the thesis, the dynamic motion part of the map got excluded as well as the map creation part.
This node aims to simply load the previously generated map for debugging purposes of the localisation method.

# Usage
TODO
