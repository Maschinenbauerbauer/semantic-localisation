import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data

from std_msgs.msg import String
from sensor_msgs.msg import Image, CameraInfo, PointCloud2


class Remapper(Node):
    
    """
    Redistributes the topics coming from the gazebo sim. 
    It was not possible to rename the topics in a fast way, so this is the most simple way.
    """

    def __init__(self):
        super().__init__('remapper')
        self.subscriber_image = self.create_subscription(Image, '/zed2_image/image_raw', self.image_cb, qos_profile_sensor_data)
        self.pub_image = self.create_publisher(Image, '/zed2/zed_node/left_raw/image_raw_color', 1)
        self.subscriber_image_camera_info= self.create_subscription(CameraInfo, '/zed2_image/camera_info', self.image_info, qos_profile_sensor_data)
        self.pub_info = self.create_publisher(CameraInfo,  '/zed2/zed_node/left_raw/camera_info', 1)
        self.subscriber_pointcloud = self.create_subscription(PointCloud2, 'zed2_depth/points', self.image_points, qos_profile_sensor_data)
        self.pub_points = self.create_publisher(PointCloud2,  '/zed2/zed_node/point_cloud/cloud_registered', 1)
        self.get_logger().debug('Remapper running..')
        pass

    def image_cb(self, msg):
        self.pub_image.publish(msg)
        pass

    def image_info(self, msg):        
        self.pub_info.publish(msg)
        pass

    def image_points(self, msg):        
        self.pub_points.publish(msg)
        pass


def main(args=None):
    rclpy.init(args=args)

    node = Remapper()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()