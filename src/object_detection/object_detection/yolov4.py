from pickle import FALSE
import sys
import os
from time import sleep

import rclpy
from rclpy.qos import qos_profile_sensor_data
from rclpy.node import Node

import cv2
from cv_bridge import CvBridge

from std_msgs.msg import String
from sensor_msgs.msg import Image
#from perception_msgs.msg import object_array, object_information
print('Dont forget to update the absolute workspace path in the yolo.py script!')
print('Dont forget to set the absolute path in the cfg/coco.data!')
ws_path = '/home/andreas/ma_ws'

sys.path.append('{}/src/object_detection/darknet'.format(ws_path))

#https://answers.ros.org/question/367793/including-a-python-module-in-a-ros2-package/
import darknet as dn

class yolo_detector(Node):

    def __init__(self, image_topic, cfg, data, weights, thresh):
        
        super().__init__('yolo_detector_node')
        
        self.bbox_publisher_ = self.create_publisher(Image, "/detections_yolo/image", 1)

        self.debugger_ = self.create_publisher(String, "/debugger", 10)
        
        self.subscriber_ = self.subscription = self.create_subscription(
            Image,
            image_topic,
            self.detect,
            qos_profile_sensor_data
            )
        
        self.cvb = CvBridge()
        
        self.thresh = thresh
        self.net, self.class_names, self.net_colors = dn.load_network(cfg, data, weights)   
        self.net_width = dn.network_width(self.net)
        self.net_height = dn.network_height(self.net)

        self.has_run = FALSE


    def detect(self, msg):
        image_rgb = self.cvb.imgmsg_to_cv2(msg, desired_encoding="rgb8") # color image with red-green-blue color order
        img_height, img_width, _ = image_rgb.shape

        if not self.has_run:
            self.get_logger().info('Received imagesize: "{}x{}"'.format(img_width, img_height))
            self.has_run = True
            
        # Only if the images changes, run darknet calculations!                
        image_resized = cv2.resize(image_rgb, (self.net_width, self.net_height), interpolation=cv2.INTER_LINEAR)
        darknet_image = dn.make_image(self.net_width, self.net_height, 3)
        dn.copy_image_from_bytes(darknet_image, image_resized.tobytes())
        detections = dn.detect_image(self.net, self.class_names, darknet_image, thresh=self.thresh)
        
        dn.free_image(darknet_image)

        # Draw the bounding boxes onto restretched_image
        bbox_image = cv2.cvtColor(dn.draw_boxes(detections, image_resized, self.net_colors), cv2.COLOR_BGR2RGB)
        image_restretched = cv2.resize(bbox_image, (img_width, img_height), interpolation=cv2.INTER_LINEAR)
        self.bbox_publisher_.publish(self.cvb.cv2_to_imgmsg(image_restretched))

        #recognized_objects = object_array()
        #recognized_objects.header = data.header
        """
        for objects in detections: # slow!
            
            object_class = objects[0]
            
            confidence = objects[1]
            
            x, y, w, h = objects[2]
            center_x = int(x / self.net_width  * img_width)
            center_y = int(y / self.net_height * img_height)
            width    = int(w / self.net_width  * img_width)
            height   = int(h / self.net_height * img_height)

            tl_x = int(center_x - width/2) # top left x
            tl_y = int(center_y - height/2) # top left y
            br_x = int(center_x + width/2) # bottom right x
            br_y = int(center_y + height/2) # bottom right y
            
            #detected_object = object_information()
            #detected_object.semantics.append(object_class)
            #detected_object.confidence = float(confidence)
            #detected_object.boundingbox = [tl_x, tl_y, br_x, br_y]
            #recognized_objects.objects.append(detected_object)

            if(self.show_image):

                cv2.rectangle(image_rgb, (tl_x, tl_y), (br_x, br_y), [0, 255, 0], 3, 3)
                cv2.putText(image_rgb, object_class, (tl_x, tl_y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, [0, 255, 0], 2) 

        if(self.show_image):
            cv2.imshow("Detections", image_rgb)
            cv2.waitKey(1) 
        """

        
def main(args=None):
    
    rclpy.init(args=args)
    
    image_topic = "camera/color/image_raw"

    thresh = 0.5
    cfg = "{}/src/object_detection/darknet/cfg/yolov4-tiny.cfg".format(ws_path)
    data = "{}/src/object_detection/darknet/cfg/coco.data".format(ws_path)
    weights = "{}/src/object_detection/darknet/yolov4-tiny.weights".format(ws_path)
    
    detector = yolo_detector(image_topic, cfg, data, weights, thresh)

    rclpy.spin(detector)
    
    detector.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
